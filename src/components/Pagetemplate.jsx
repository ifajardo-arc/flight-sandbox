import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Component Name</h1>
                  <p className="pb-3 type-heading-h3-secondary">Component Description</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  component visual
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Card from "react-bootstrap/Card";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Card>
  <Card.Body>
    <Card.Title className="type-heading-h1">Card Title</Card.Title>
    <Card.Text className="type-body-primary-on-light">
      Some quick example text to build on the card title and make
      up the bulk of the card's content.
    </Card.Text>
    <Button variant="primary">Click Here</Button>
  </Card.Body>
</Card>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div className="card">
  <div className="card-body">
    <h1 className="type-heading-h1">Card Title</h1>
    <p className="type-body-primary-on-light">Some quick example text to build on the card title and make
      up the bulk of the card's content.</p>
    <Button variant="primary">Click Here</Button>
  </div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />  
          </div> 
        </div>
      </div>
    
    );
  }
}

export default FDSDropdown;

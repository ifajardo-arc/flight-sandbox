import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "./Bootstrapnav";
import Sidebar from "./Sidebar";

class FDSTypography extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Typography</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    FDS-Bootstrap includes generated typography classes included
                    with the flight-dev-kit. These can be referenced{" "}
                    <a
                      href="https://flight-ds.netlify.com/guidelines/typography/design-tokens"
                      target="_blank"
                    >
                      here
                    </a>
                    . Each token can be used as a class by replacing the $ with
                    a .
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <code className="language-scss">{`$type-heading-h1-xl	`}</code>
                  <br />
                  <br />
                  <code className="language-html">{`.type-heading-h1-xl`}</code>
                  <br />
                  <br />
                  <h1 className="type-heading-h1-xl">
                    This is header with type-heading-h1-xl class
                  </h1>
                </div>
                <div className="col-md-4">
                  <code className="language-scss">{`$type-heading-h2`}</code>
                  <br />
                  <br />
                  <code className="language-html">{`.type-heading-h2`}</code>
                  <br />
                  <br />
                  <h2 className="type-heading-h2">
                    This is header with type-heading-h2 class
                  </h2>
                </div>
                <div className="col-md-4">
                  <code className="language-scss">{`$type-body-primary-on-light	`}</code>
                  <br />
                  <br />
                  <code className="language-html">{`.type-body-primary-on-light`}</code>
                  <br />
                  <br />
                  <p className="type-body-primary-on-light">
                    This is header with type-body-primary-on-light class
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="html"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="html" title="HTML">
                      <code className="language-javascript">
                        {`@import "../../node_modules/flight-dev-kit/dist/scss/partials/design-tokens/typography";`}
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<h1 className="type-heading-h1-xl">Card Title</h1>

<h1 className="type-heading-h1">Card Title</h1>

<h2 className="type-heading-h2">Card Title</h2>

<h3 className="type-heading-h3-primary">Card Title</h3>

<h3 className="type-heading-h3-secondary">Card Title</h3>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSTypography;

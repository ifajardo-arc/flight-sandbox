import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";
import ProgressBar from "react-bootstrap/ProgressBar";

class FDSProgressbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toastDisplay: true
    };

    this.toggleToast = this.toggleToast.bind(this);
  }

  toggleToast() {
    if (this.state.toastDisplay === false) {
      this.setState({ toastDisplay: true });
    } else {
      this.setState({ toastDisplay: false });
    }
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    const now = 60;

    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Progress Bar</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Provide up-to-date feedback on the progress of a workflow or
                    action with simple yet flexible progress bars.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <ProgressBar now={30} />

                  <br />

                  <ProgressBar now={now} label={`${now}%`} />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Card from "react-bootstrap/ProgressBar";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<ProgressBar now={30} />

<ProgressBar now={now} label={\`${now}%\`} />`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
</div>

<div class="progress">
  <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSProgressbar;

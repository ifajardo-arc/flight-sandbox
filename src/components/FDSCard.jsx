import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Card</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Cards are a highly flexible component for displaying a wide
                    variety of content. They can range from basic, text-only
                    cards to complex cards containing many other Flight
                    components. The card itself is nothing more than a rectangle
                    or square that responds to the grid. The only pre-set styles
                    are the elevation and radius tokens used. Beyond that, you
                    can customize the cards content and design to meet your
                    specific needs. Just be sure to leverage the other Flight
                    design tokens for typography, space and color.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <Card>
                    <Card.Body>
                      <Card.Title className="type-heading-h1">
                        Card Title
                      </Card.Title>
                      <Card.Text className="type-body-primary-on-light">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </Card.Text>
                      <Button variant="primary">Click Here</Button>
                    </Card.Body>
                  </Card>
                </div>
                <div className="col-md-4">
                  <div className="card">
                    <div className="card-body">
                      <h1 className="type-heading-h1">Card Title</h1>
                      <p className="type-body-primary-on-light">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <Button variant="primary">Click Here</Button>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card">
                    <div className="card-body">
                      <h1 className="type-heading-h1">Card Title</h1>
                      <p className="type-body-primary-on-light">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <Button variant="primary">Click Here</Button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row mt-5">
                <div className="col-md-6">
                  <Card>
                    <Card.Img
                      variant="top"
                      src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1701b217bf2%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3A-apple-system%2CBlinkMacSystemFont%2C%26quot%3BSegoe%20UI%26quot%3B%2CRoboto%2C%26quot%3BHelvetica%20Neue%26quot%3B%2CArial%2C%26quot%3BNoto%20Sans%26quot%3B%2Csans-serif%2C%26quot%3BApple%20Color%20Emoji%26quot%3B%2C%26quot%3BSegoe%20UI%20Emoji%26quot%3B%2C%26quot%3BSegoe%20UI%20Symbol%26quot%3B%2C%26quot%3BNoto%20Color%20Emoji%26quot%3B%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1701b217bf2%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22101.5546875%22%20y%3D%2296.6%22%3EImage%20cap%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                    />
                    <Card.Body>
                      <Card.Title className="type-heading-h1">
                        Card Title
                      </Card.Title>
                      <Card.Text className="type-body-primary-on-light">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </Card.Text>
                    </Card.Body>
                    <ListGroup className="list-group-flush">
                      <ListGroupItem>Cras justo odio</ListGroupItem>
                      <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
                      <ListGroupItem>Vestibulum at eros</ListGroupItem>
                    </ListGroup>
                    <Card.Body>
                      <Card.Link href="#">Card Link</Card.Link>
                      <Card.Link href="#">Another Link</Card.Link>
                    </Card.Body>
                  </Card>
                </div>
                <div className="col-md-6">
                  <Card>
                    <Card.Header className="type-heading-h3-secondary">
                      Featured
                    </Card.Header>
                    <Card.Body>
                      <Card.Title className="type-heading-h2">
                        Special title treatment
                      </Card.Title>
                      <Card.Text>
                        With supporting text below as a natural lead-in to
                        additional content.
                      </Card.Text>

                      <Button variant="primary">Go somewhere</Button>
                    </Card.Body>
                    <Card.Footer className="text-muted">2 days ago</Card.Footer>
                  </Card>
                </div>
              </div>
              <div className="row mt-5">
                <div className="col-md-6">
                  <Card>
                    <Card.Body>
                      <Tabs
                        defaultActiveKey="profile"
                        id="uncontrolled-tab-example"
                        className="justify-content-center"
                      >
                        <Tab eventKey="home" title="Home" >
                          <Card.Title className="type-heading-h1 mt-3">Special title treatment</Card.Title>
                          <Card.Text>
                            With supporting text below as a natural lead-in to
                            additional content.
                          </Card.Text>
                          <Button variant="primary">Go somewhere</Button>
                        </Tab>
                        <Tab eventKey="profile" title="Profile">
                          <Card.Title className="type-heading-h1 mt-3">Profile 1</Card.Title>
                          <Card.Text>
                            With supporting text below as a natural lead-in to
                            additional content.
                          </Card.Text>
                          <Button variant="primary">Go somewhere</Button>
                        </Tab>
                        <Tab eventKey="contact" title="Contact" disabled>
                          <Card.Title>Special title treatment</Card.Title>
                          <Card.Text>
                            With supporting text below as a natural lead-in to
                            additional content.
                          </Card.Text>
                          <Button variant="primary">Go somewhere</Button>
                        </Tab>
                      </Tabs>
                    </Card.Body>
                  </Card>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Card from "react-bootstrap/Card";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Card>
  <Card.Body>
    <Card.Title className="type-heading-h1">Card Title</Card.Title>
    <Card.Text className="type-body-primary-on-light">
      Some quick example text to build on the card title and make
      up the bulk of the card's content.
    </Card.Text>
    <Button variant="primary">Click Here</Button>
  </Card.Body>
</Card>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div className="card">
  <div className="card-body">
    <h1 className="type-heading-h1">Card Title</h1>
    <p className="type-body-primary-on-light">Some quick example text to build on the card title and make
      up the bulk of the card's content.</p>
    <Button variant="primary">Click Here</Button>
  </div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSCard;

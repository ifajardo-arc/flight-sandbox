import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSTokens extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Design Tokens</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Our design tokens can be used to keep interfaces consistent
                    with our design system. These include color, spacing,
                    elevation and more. Tokens should be included as a SCSS
                    partial for resuability within your application.  For example, the base spacing token can be used to keep the spacing within our 8px/16px spacing convention.
                  </p>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  Documentation for tokens can be viewed here:
                </div>
                <div className="col-md-12">
                  <ul class="fds-list fds-list--bulleted">
                    <li class="fds-list__item"><a
                    target="_blank"
                    href="https://flightds.netlify.com/guidelines/color/design-tokens"
                  >
                    Color
                  </a></li>
                    <li class="fds-list__item"><a
                    target="_blank"
                    href="https://flightds.netlify.com/guidelines/elevation/design-tokens"
                  >
                    Elevation
                  </a></li>
                    <li class="fds-list__item"><a
                    target="_blank"
                    href="https://flightds.netlify.com/guidelines/spacing/design-tokens"
                  >
                    Spacing
                  </a></li>
                    <li class="fds-list__item"><a
                    target="_blank"
                    href="https://flightds.netlify.com/guidelines/radius/design-tokens"
                  >
                    Radius
                  </a></li>
                  </ul>
                  
                  
                  
                  
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="sass"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="sass" title="SCSS">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-scss">
                          {`@import "../../node_modules/fds-bootstrap/dist/color.scss";
@import "../../node_modules/fds-bootstrap/dist/elevation.scss";
@import "../../node_modules/fds-bootstrap/dist/radius.scss";
@import "../../node_modules/fds-bootstrap/dist/space.scss";
@import "../../node_modules/fds-bootstrap/dist/typography.scss";

/* calculating custom spacing */
$custom-space: $space-sm-fixed * 5;

.custom-component {
  background: $teal-500;
  padding: $space-sm-fixed;
  border-radius: $rad-md;
  box-shadow: $ele-lg;
  margin-bottom: $custom-space;
}
`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSTokens;

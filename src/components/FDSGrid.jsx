import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Grid</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    FDS Bootstrap’s grid system uses a series of containers,
                    rows, and columns to layout and align content. It’s built
                    with flexbox and is fully responsive. Below is an example
                    and an in-depth look at how the grid comes together.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <h3 className="type-heading-h3-primary mt-5">
                    Auto-layout columns
                  </h3>
                  <p>
                    When no column widths are specified the <code className="language-html">{`Col`}</code> component will
                    render equal width columns
                  </p>
                  <Container>
                    <Row>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                      >
                        1 of 2
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                      >
                        2 of 2
                      </Col>
                    </Row>
                    <Row>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                      >
                        1 of 3
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                      >
                        2 of 3
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                      >
                        3 of 3
                      </Col>
                    </Row>
                  </Container>

                  <h3 className="type-heading-h3-primary mt-5">
                    Responsive grids
                  </h3>
                  <p>
                    The Col lets you specify column widths across 5 breakpoint
                    sizes (xs, sm, md, large, and xl). For every breakpoint, you
                    can specify the amount of columns to span, or set the prop
                    to <code className="language-jsx">{`<Col lg={true} />`}</code> for auto layout
                    widths.
                  </p>
                  <Container>
                    {/* Stack the columns on mobile by making one full-width and the other half-width */}
                    <Row>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={12}
                        md={8}
                      >
                        xs=12 md=8
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={6}
                        md={4}
                      >
                        xs=6 md=4
                      </Col>
                    </Row>

                    {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}
                    <Row>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={6}
                        md={4}
                      >
                        xs=6 md=4
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={6}
                        md={4}
                      >
                        xs=6 md=4
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={6}
                        md={4}
                      >
                        xs=6 md=4
                      </Col>
                    </Row>

                    {/* Columns are always 50% wide, on mobile and desktop */}
                    <Row>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={6}
                      >
                        xs=6
                      </Col>
                      <Col
                        style={{ border: "1px solid #000000", padding: "5px" }}
                        xs={6}
                      >
                        xs=6
                      </Col>
                    </Row>
                  </Container>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <pre>
                        <code className="language-javascript">
                          {`import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row"; 
import Col from "react-bootstrap/Col";`}
                        </code>
                      </pre>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Container>
  <Row>
    <Col>1 of 2</Col>
    <Col>2 of 2</Col>
  </Row>
  <Row>
    <Col>1 of 3</Col>
    <Col>2 of 3</Col>
    <Col>3 of 3</Col>
  </Row>
</Container>

<Container>
  {/* Stack the columns on mobile by making one full-width and the other half-width */}
  <Row>
    <Col xs={12} md={8}>
      xs=12 md=8
    </Col>
    <Col xs={6} md={4}>
      xs=6 md=4
    </Col>
  </Row>

  {/* Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop */}
  <Row>
    <Col xs={6} md={4}>
      xs=6 md=4
    </Col>
    <Col xs={6} md={4}>
      xs=6 md=4
    </Col>
    <Col xs={6} md={4}>
      xs=6 md=4
    </Col>
  </Row>

  {/* Columns are always 50% wide, on mobile and desktop */}
  <Row>
    <Col xs={6}>xs=6</Col>
    <Col xs={6}>xs=6</Col>
  </Row>
</Container>


`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div class="container">
  <div class="row">
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      One of three columns
    </div>
  </div>
</div>

<div class="container">
  <!-- Stack the columns on mobile by making one full-width and the other half-width -->
  <div class="row">
    <div class="col-12 col-md-8">.col-12 .col-md-8</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  </div>

  <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
  <div class="row">
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  </div>

  <!-- Columns are always 50% wide, on mobile and desktop -->
  <div class="row">
    <div class="col-6">.col-6</div>
    <div class="col-6">.col-6</div>
  </div>
</div>
`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSGrid;

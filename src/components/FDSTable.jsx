import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Table from "react-bootstrap/Table";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Table</h1>
                  <p className="pb-3 type-heading-h3-secondary">The table component is in its MVP state as of now. The documentation shown here is meant to illustrate how the table component will work from a conceptual level and to demonstrate how Flight design tokens are applied. As more user testing is done, we anticipate the table component will evolve. Additionally, if an existing data table framework will be leveraged by the implementation team, that decision will likely have an impact on the table component's design to some degree. With that said, let's take a look at the pieces that make up the table component.</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <Table >
                    <thead>
                      <tr>
                        <th>Header</th>
                        <th>Header</th>
                        <th>Header</th>
                        <th>Header</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Data cell</td>
                        <td>Data cell</td>
                        <td>Data cell</td>
                        <td>Data cell</td>
                      </tr>
                      <tr>
                        <td>Data cell</td>
                        <td>Data cell</td>
                        <td>Data cell</td>
                        <td>Data cell</td>
                      </tr>
                      <tr>
                        <td>Data cell</td>
                        <td>Data cell</td>
                        <td>Data cell</td>
                        <td>Data cell</td>
                      </tr>
                    </tbody>
                  </Table>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Table from "react-bootstrap/Table";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Table>
  <thead>
    <tr>
      <th>Header</th>
      <th>Header</th>
      <th>Header</th>
      <th>Header</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Data cell</td>
      <td>Data cell</td>
      <td>Data cell</td>
      <td>Data cell</td>
    </tr>
    <tr>
      <td>Data cell</td>
      <td>Data cell</td>
      <td>Data cell</td>
      <td>Data cell</td>
    </tr>
    <tr>
      <td>Data cell</td>
      <td>Data cell</td>
      <td>Data cell</td>
      <td>Data cell</td>
    </tr>
  </tbody>
</Table>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<table class="table">
  <thead>
  <tr>
    <th>Header</th>
    <th>Header</th>
    <th>Header</th>
    <th>Header</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Data cell</td>
    <td>Data cell</td>
    <td>Data cell</td>
    <td>Data cell</td>
  </tr>
  <tr>
    <td>Data cell</td>
    <td>Data cell</td>
    <td>Data cell</td>
    <td>Data cell</td>
  </tr>
  <tr>
    <td>Data cell</td>
    <td>Data cell</td>
    <td>Data cell</td>
    <td>Data cell</td>
  </tr>
</tbody>
</table>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />  
          </div> 
        </div>
      </div>
    );
  }
}

export default FDSTable;

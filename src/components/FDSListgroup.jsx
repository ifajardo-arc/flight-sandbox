import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import ListGroup from "react-bootstrap/ListGroup";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSListgroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">List Group</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    List groups are a flexible and powerful component for
                    displaying a series of content. Modify and extend them to
                    support just about any content within.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <h2 className="type-heading-h2">Basic</h2>
                  <ListGroup>
                    <ListGroup.Item>Cras justo odio</ListGroup.Item>
                    <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
                    <ListGroup.Item>Morbi leo risus</ListGroup.Item>
                    <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
                    <ListGroup.Item>Vestibulum at eros</ListGroup.Item>
                  </ListGroup>
                </div>
                <div className="col-md-4">
                  <h2 className="type-heading-h2">Active</h2>
                  <ListGroup>
                    <ListGroup.Item active>Cras justo odio</ListGroup.Item>
                    <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
                    <ListGroup.Item disabled>Morbi leo risus</ListGroup.Item>
                    <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
                  </ListGroup>
                </div>
                <div className="col-md-4">
                  <h2 className="type-heading-h2">Flush</h2>
                  <ListGroup variant="flush">
                    <ListGroup.Item>Cras justo odio</ListGroup.Item>
                    <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
                    <ListGroup.Item>Morbi leo risus</ListGroup.Item>
                    <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
                  </ListGroup>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import ListGroup from "react-bootstrap/ListGroup";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`//Basic
<ListGroup>
  <ListGroup.Item>Cras justo odio</ListGroup.Item>
  <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
  <ListGroup.Item>Morbi leo risus</ListGroup.Item>
  <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
  <ListGroup.Item>Vestibulum at eros</ListGroup.Item>
</ListGroup>

//Active
<ListGroup>
  <ListGroup.Item active>
    Cras justo odio
  </ListGroup.Item>
  <ListGroup.Item>
    Dapibus ac facilisis in
  </ListGroup.Item>
  <ListGroup.Item disabled>
    Morbi leo risus
  </ListGroup.Item>
  <ListGroup.Item>
    Porta ac consectetur ac
  </ListGroup.Item>
</ListGroup>

//Flush
<ListGroup variant="flush">
  <ListGroup.Item>Cras justo odio</ListGroup.Item>
  <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
  <ListGroup.Item>Morbi leo risus</ListGroup.Item>
  <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
</ListGroup>
                          `}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<!-- Basic -->
<div class="list-group">
  <div class="list-group-item">Cras justo odio</div>
  <div class="list-group-item">Dapibus ac facilisis in</div>
  <div class="list-group-item">Morbi leo risus</div>
  <div class="list-group-item">Porta ac consectetur ac</div>
  <div class="list-group-item">Vestibulum at eros</div>
</div>

<!-- Active -->
<div class="list-group">
  <div class="list-group-item active">Cras justo odio</div>
  <div class="list-group-item">Dapibus ac facilisis in</div>
  <div class="list-group-item">Morbi leo risus</div>
  <div class="list-group-item">Porta ac consectetur ac</div>
  <div class="list-group-item">Vestibulum at eros</div>
</div>

<!-- Flush -->
<div class="list-group list-group-flush">
  <div class="list-group-item">Cras justo odio</div>
  <div class="list-group-item">Dapibus ac facilisis in</div>
  <div class="list-group-item">Morbi leo risus</div>
  <div class="list-group-item">Porta ac consectetur ac</div>
  <div class="list-group-item">Vestibulum at eros</div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSListgroup;

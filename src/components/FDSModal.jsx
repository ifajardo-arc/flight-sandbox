import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Toast from "react-bootstrap/Toast";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      confirmation: false,
      destructive: false,
      input: false
    };

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.showConfirmation = this.showConfirmation.bind(this);
    this.hideConfirmation = this.hideConfirmation.bind(this);
    this.showDestructive = this.showDestructive.bind(this);
    this.hideDestructive = this.hideDestructive.bind(this);
    this.showInput = this.showInput.bind(this);
    this.hideInput = this.hideInput.bind(this);
  }

  handleShow() {
    this.setState({ show: true });
  }

  handleClose() {
    this.setState({ show: false });
  }

  showConfirmation() {
    this.setState({ confirmation: true });
  }

  hideConfirmation() {
    this.setState({ confirmation: false });
  }

  showDestructive() {
    this.setState({ destructive: true });
  }

  hideDestructive() {
    this.setState({ destructive: false });
  }

  showInput() {
    this.setState({ input: true });
  }

  hideInput() {
    this.setState({ input: false });
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Modal</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Modals interrupt the current workflow to communicate and/or
                    collect important infomation within the context of the task
                    at hand.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <h2 className="type-heading-h2 mb-3">Interruption Modal</h2>
                  <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                      <div className="modal-label">Modal label (optional)</div>
                      <Modal.Title>Interruption Modal</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
                      diam donec adipiscing tristique risus. Risus pretium quam
                      vulputate dignissim suspendisse in est. Egestas dui id
                      ornare arcu. Fames ac turpis egestas integer eget aliquet.
                      Ut diam quam nulla porttitor massa. Quis vel eros donec ac
                      odio tempor orci dapibus. Vitae sapien pellentesque
                      habitant morbi tristique senectus et netus et. Scelerisque
                      eu ultrices vitae auctor eu augue ut. Tortor at auctor
                      urna nunc id. In arcu cursus euismod quis viverra nibh
                      cras pulvinar mattis. Arcu dictum varius duis at
                      consectetur lorem donec.
                    </Modal.Body>
                  </Modal>

                  <button onClick={this.handleShow} className="btn btn-primary">
                    Show Interruption Modal
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Modal from "react-bootstrap/Modal";
                      </code>
                      <pre style={{ padding: "2rem", maxHeight: "300px" }}>
                        <code className="language-jsx">
                          {`<Modal show={this.state.show} onHide={this.handleClose}>
  <Modal.Header closeButton>
    <div className="modal-label">Modal label (optional)</div>
    <Modal.Title>Interruption Modal</Modal.Title>
  </Modal.Header>
  <Modal.Body>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna
    aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
    diam donec adipiscing tristique risus. Risus pretium quam
    vulputate dignissim suspendisse in est. Egestas dui id
    ornare arcu. Fames ac turpis egestas integer eget aliquet.
    Ut diam quam nulla porttitor massa. Quis vel eros donec ac
    odio tempor orci dapibus. Vitae sapien pellentesque
    habitant morbi tristique senectus et netus et. Scelerisque
    eu ultrices vitae auctor eu augue ut. Tortor at auctor
    urna nunc id. In arcu cursus euismod quis viverra nibh
    cras pulvinar mattis. Arcu dictum varius duis at
    consectetur lorem donec.
  </Modal.Body>
</Modal>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <h2 className="type-heading-h2 mb-3 mt-5">
                    Confirmation Modal
                  </h2>
                  <Modal
                    show={this.state.confirmation}
                    onHide={this.hideConfirmation}
                  >
                    <Modal.Header closeButton>
                      <Modal.Title>Interruption Modal</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
                      diam donec adipiscing tristique risus. Risus pretium quam
                      vulputate dignissim suspendisse in est. Egestas dui id
                      ornare arcu. Fames ac turpis egestas integer eget aliquet.
                      Ut diam quam nulla porttitor massa. Quis vel eros donec ac
                      odio tempor orci dapibus. Vitae sapien pellentesque
                      habitant morbi tristique senectus et netus et. Scelerisque
                      eu ultrices vitae auctor eu augue ut. Tortor at auctor
                      urna nunc id. In arcu cursus euismod quis viverra nibh
                      cras pulvinar mattis. Arcu dictum varius duis at
                      consectetur lorem donec.
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="primary" onClick={this.hideConfirmation}>
                        Primary Action
                      </Button>
                      <Button
                        variant="secondary"
                        onClick={this.hideConfirmation}
                      >
                        Secondary Action
                      </Button>
                    </Modal.Footer>
                  </Modal>

                  <button
                    onClick={this.showConfirmation}
                    className="btn btn-primary"
                  >
                    Show Confirmation Modal
                  </button>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Modal from "react-bootstrap/Modal";
                      </code>
                      <pre style={{ padding: "2rem", maxHeight: "300px" }}>
                        <code className="language-jsx">
                          {`
    <Modal
      show={this.state.confirmation}
      onHide={this.hideConfirmation}
    >
      <Modal.Header closeButton>
        <Modal.Title>Interruption Modal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
        diam donec adipiscing tristique risus. Risus pretium quam
        vulputate dignissim suspendisse in est. Egestas dui id
        ornare arcu. Fames ac turpis egestas integer eget aliquet.
        Ut diam quam nulla porttitor massa. Quis vel eros donec ac
        odio tempor orci dapibus. Vitae sapien pellentesque
        habitant morbi tristique senectus et netus et. Scelerisque
        eu ultrices vitae auctor eu augue ut. Tortor at auctor
        urna nunc id. In arcu cursus euismod quis viverra nibh
        cras pulvinar mattis. Arcu dictum varius duis at
        consectetur lorem donec.
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={this.hideConfirmation}>
          Primary Action
        </Button>
        <Button
          variant="secondary"
          onClick={this.hideConfirmation}
        >
          Secondary Action
        </Button>
      </Modal.Footer>
    </Modal>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <h2 className="type-heading-h2 mb-3 mt-5">
                    Destructive Modal
                  </h2>
                  <Modal
                    show={this.state.destructive}
                    onHide={this.hideDestructive}
                  >
                    <Modal.Header closeButton>
                      <Modal.Title>Destructive Modal</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
                      diam donec adipiscing tristique risus. Risus pretium quam
                      vulputate dignissim suspendisse in est. Egestas dui id
                      ornare arcu. Fames ac turpis egestas integer eget aliquet.
                      Ut diam quam nulla porttitor massa. Quis vel eros donec ac
                      odio tempor orci dapibus. Vitae sapien pellentesque
                      habitant morbi tristique senectus et netus et. Scelerisque
                      eu ultrices vitae auctor eu augue ut. Tortor at auctor
                      urna nunc id. In arcu cursus euismod quis viverra nibh
                      cras pulvinar mattis. Arcu dictum varius duis at
                      consectetur lorem donec.
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="danger" onClick={this.hideDestructive}>
                        Destructive Action
                      </Button>
                      <Button
                        variant="secondary"
                        onClick={this.hideDestructive}
                      >
                        Secondary Action
                      </Button>
                    </Modal.Footer>
                  </Modal>

                  <button
                    onClick={this.showDestructive}
                    className="btn btn-primary"
                  >
                    Show Destructive Modal
                  </button>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Modal from "react-bootstrap/Modal";
                      </code>
                      <pre style={{ padding: "2rem", maxHeight: "300px" }}>
                        <code className="language-jsx">
                          {`<Modal
      show={this.state.destructive}
      onHide={this.hideDestructive}
    >
      <Modal.Header closeButton>
        <Modal.Title>Interruption Modal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
        diam donec adipiscing tristique risus. Risus pretium quam
        vulputate dignissim suspendisse in est. Egestas dui id
        ornare arcu. Fames ac turpis egestas integer eget aliquet.
        Ut diam quam nulla porttitor massa. Quis vel eros donec ac
        odio tempor orci dapibus. Vitae sapien pellentesque
        habitant morbi tristique senectus et netus et. Scelerisque
        eu ultrices vitae auctor eu augue ut. Tortor at auctor
        urna nunc id. In arcu cursus euismod quis viverra nibh
        cras pulvinar mattis. Arcu dictum varius duis at
        consectetur lorem donec.
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={this.hideDestructive}>
          Destructive Action
        </Button>
        <Button
          variant="secondary"
          onClick={this.hideDestructive}
        >
          Secondary Action
        </Button>
      </Modal.Footer>
    </Modal>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12">
                  <h2 className="type-heading-h2 mb-3 mt-5">Input Modal</h2>
                  <Modal show={this.state.input} onHide={this.hideInput}>
                    <Modal.Header closeButton>
                      <Modal.Title>Input Modal</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <p>
                        Add your preferred input method components and resize
                        this modal symbol instance to fit your design.
                      </p>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                        <Form.Text className="text-muted">
                          We'll never share your email with anyone else.
                        </Form.Text>
                      </Form.Group>
                      <Form.Group id="formGridCheckbox">
                        <Form.Check type="checkbox" label="I agree to the updated terms of service" />
                        
                      </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                      <Button variant="primary" onClick={this.hideInput}>
                        Primary Action
                      </Button>
                      <Button variant="secondary" onClick={this.hideInput}>
                        Secondary Action
                      </Button>
                    </Modal.Footer>
                  </Modal>

                  <button onClick={this.showInput} className="btn btn-primary">
                    Show Input Modal
                  </button>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Modal from "react-bootstrap/Modal";
                      </code>
                      <pre style={{ padding: "2rem", maxHeight: "300px" }}>
                        <code className="language-jsx">
                          {`<Modal
      show={this.state.destructive}
      onHide={this.hideDestructive}
    >
      <Modal.Header closeButton>
        <Modal.Title>Interruption Modal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna
        aliqua. Volutpat ac tincidunt vitae semper. Sed adipiscing
        diam donec adipiscing tristique risus. Risus pretium quam
        vulputate dignissim suspendisse in est. Egestas dui id
        ornare arcu. Fames ac turpis egestas integer eget aliquet.
        Ut diam quam nulla porttitor massa. Quis vel eros donec ac
        odio tempor orci dapibus. Vitae sapien pellentesque
        habitant morbi tristique senectus et netus et. Scelerisque
        eu ultrices vitae auctor eu augue ut. Tortor at auctor
        urna nunc id. In arcu cursus euismod quis viverra nibh
        cras pulvinar mattis. Arcu dictum varius duis at
        consectetur lorem donec.
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={this.hideDestructive}>
          Destructive Action
        </Button>
        <Button
          variant="secondary"
          onClick={this.hideDestructive}
        >
          Secondary Action
        </Button>
      </Modal.Footer>
    </Modal>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSModal;

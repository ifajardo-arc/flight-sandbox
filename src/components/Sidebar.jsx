import React, { Component } from "react";
import { HashRouter as Router, Route, Link } from "react-router-dom";

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="resources-sidebar px-3 py-5">
        <h3 className="type-caption-on-light">Resources</h3>
        <a className="resources-sidebar-link" target="_blank" href="https://flightds.netlify.com/">Flight Design System</a>
        <a className="resources-sidebar-link" target="_blank" href="https://www2.arccorp.com/styleguide">Marketing Style Guide</a>
        <a className="resources-sidebar-link" target="_blank" href="https://www2.arccorp.com/about-us/our-technology">Our Technology</a>
      
        
      </div>
    );
  }
}

export default Sidebar;

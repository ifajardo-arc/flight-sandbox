import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSForms extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Forms</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Examples and usage guidelines for form control styles,
                    layout options, and custom components for creating a wide
                    variety of forms.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary">
                    Text Input & Text Area
                  </h3>
                  <p>
                    The Text Input component gives users the ability to enter
                    and edit text. It is used for long form and short form
                    content. Use the text input component when you expect the
                    user to input a single line entry and the text area
                    component for entries longer than one sentence.
                  </p>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                    <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text>
                  </Form.Group>
                </div>
                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary">Select</h3>
                  <p>
                    The select component gives users the ability to submit data
                    within forms by choosing one option from a list.In the
                    future, more types may be needed as components are added to
                    Flight. (e.g. Small and Inline selects)
                  </p>
                  <Form>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label>Example select</Form.Label>
                      <Form.Control as="select">
                        <option>Select Option 1</option>
                        <option>Select Option 2</option>
                        <option>Select Option 3</option>
                        <option>Select Option 4</option>
                        <option>Select Option 5</option>
                      </Form.Control>
                    </Form.Group>
                  </Form>
                </div>
              </div>

              <div className="row">
                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary">Radio & Checkbox</h3>
                  <Form>
                    {["checkbox", "radio"].map(type => (
                      <div key={`default-${type}`} className="mb-3">
                        <Form.Check
                          type={type}
                          id={`default-${type}`}
                          label={`default ${type}`}
                        />

                        <Form.Check
                          disabled
                          type={type}
                          label={`disabled ${type}`}
                          id={`disabled-default-${type}`}
                        />
                      </div>
                    ))}
                  </Form>
                </div>

                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary">Button addons</h3>
                  <p>
                    A trailing icon can be displayed using{" "}
                    <code className="language-jsx">{`<InputGroup>`}</code>. If
                    desired, you can also apply one of the fds-glyphs- classes
                    to automatically pull in an icon from the fds-glyphs font.
                  </p>
                  <InputGroup className="mb-3">
                    <FormControl
                      placeholder="Recipient's username"
                      aria-label="Recipient's username"
                      aria-describedby="basic-addon2"
                    />
                    <InputGroup.Append>
                      <Button>
                        <i className="fds-button__icon fds-glyphs-search"></i>
                      </Button>
                    </InputGroup.Append>
                  </InputGroup>

                </div>
              </div>

              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <pre>
                        <code className="language-javascript">
                          {`import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";`}
                        </code>
                      </pre>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {` //Text Input
<Form>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" />
    <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  //Text Input
  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>

  //Input with button addon
  <InputGroup className="mb-3">
    <FormControl
      placeholder="Recipient's username"
      aria-label="Recipient's username"
      aria-describedby="basic-addon2"
    />
    <InputGroup.Append>
      <Button>
        <i className="fds-button__icon fds-glyphs-search"></i>
      </Button>
    </InputGroup.Append>
  </InputGroup>

  //Checkbox
  <Form.Group controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Check me out" />
  </Form.Group>
  <Form.Group controlId="exampleForm.ControlSelect2">
    <Form.Label>Example multiple select</Form.Label>
    <Form.Control as="select" multiple>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </Form.Control>
  </Form.Group>
  <Button variant="primary" type="submit">
    Submit
  </Button>
</Form>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Example select</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSForms;

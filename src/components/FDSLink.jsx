import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Link</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Links within Flight DS are most commonly used for
                    navigation. However, they may also change what or how data
                    is displayed. For example, showing more or viewing all. We
                    suggest using a button if the action will change or
                    manipulate data. The use of a ghost button might make for a
                    good alternative to a link.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <a href="#" className="fds-link">
                    Test Link
                  </a>
                </div>
                <div className="col-md-4">
                  <a href="#" className="fds-link">
                    Lorem Ipsum
                  </a>
                </div>
                <div className="col-md-4"></div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Tabs defaultActiveKey="react" id="link-example">
                    <Tab eventKey="react" title="React">
                      <pre>
                        <code className="language-jsx">
                          {`<a href="#" className="fds-link">Test Link</a>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre>
                        <code className="language-html">
                          {`<a href="#" class="fds-link">Test Link</a>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSLink;

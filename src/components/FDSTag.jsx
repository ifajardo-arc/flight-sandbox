import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSTag extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Tag</h1>
                  <p className="pb-3 type-heading-h3-secondary"> &nbsp; </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary">Basic</h3>
                  <p>
                    Tags are gray by default, with some classes defined for
                    utilizing predefined color variants.
                  </p>
                  <div className="fds-tag fds-tag--gray">Tag label</div>
                  <div className="fds-tag fds-tag--green">Tag label</div>
                  <div className="fds-tag fds-tag--teal">Tag label</div>
                  <div className="fds-tag fds-tag--red">Tag label</div>
                  <div className="fds-tag fds-tag--orange">Tag label</div>
                </div>
                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary">Interactive</h3>
                  <p>For interactive tags, an icon can be provided.</p>
                  <div className="fds-tag fds-tag--gray">
                    Tag label<i className="fds-glyphs-clear"></i>
                  </div>
                  <div className="fds-tag fds-tag--green">
                    Tag label<i className="fds-glyphs-clear"></i>
                  </div>
                  <div className="fds-tag fds-tag--teal">
                    Tag label<i className="fds-glyphs-clear"></i>
                  </div>
                  <div className="fds-tag fds-tag--red">
                    Tag label<i className="fds-glyphs-clear"></i>
                  </div>
                  <div className="fds-tag fds-tag--orange">
                    Tag label<i className="fds-glyphs-clear"></i>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="tab-code-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="Basic">
                      <code className="language-javascript">
                        import Tabs from "react-bootstrap/Tabs";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<div className="fds-tag fds-tag--gray">Tag label</div>
<div className="fds-tag fds-tag--green">Tag label</div>
<div className="fds-tag fds-tag--teal">Tag label</div>
<div className="fds-tag fds-tag--red">Tag label</div>
<div className="fds-tag fds-tag--orange">Tag label</div>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="Interactive">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div className="fds-tag fds-tag--gray">Tag label<i className="fds-glyphs-clear"></i></div>
<div className="fds-tag fds-tag--green">Tag label<i className="fds-glyphs-clear"></i></div>
<div className="fds-tag fds-tag--teal">Tag label<i className="fds-glyphs-clear"></i></div>
<div className="fds-tag fds-tag--red">Tag label<i className="fds-glyphs-clear"></i></div>
<div className="fds-tag fds-tag--orange">Tag label<i className="fds-glyphs-clear"></i></div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSTag;

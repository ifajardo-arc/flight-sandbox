import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";
import Spinner from "react-bootstrap/Spinner";

class FDSSpinners extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toastDisplay: true
    };

    this.toggleToast = this.toggleToast.bind(this);
  }

  toggleToast() {
    if (this.state.toastDisplay === false) {
      this.setState({ toastDisplay: true });
    } else {
      this.setState({ toastDisplay: false });
    }
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    const now = 60;

    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Spinners</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Indicate the loading state of a component or page with
                    spinners, built entirely with HTML, CSS, and no JavaScript.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Spinner animation="border" role="status" variant="primary">
                    <span className="sr-only">Loading...</span>
                  </Spinner>

                  <br />
                  <br />

                  <Button variant="primary">
                    <Spinner
                      as="span"
                      animation="border"
                      size="sm"
                      role="status"
                      aria-hidden="true"
                    /> &nbsp;
                    Loading...
                  </Button>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Card from "react-bootstrap/ProgressBar";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Spinner animation="border" role="status" variant="primary">
  <span className="sr-only">Loading...</span>
</Spinner>

<Button variant="primary">
  <Spinner
    as="span"
    animation="border"
    size="sm"
    role="status"
    aria-hidden="true"
  /> &nbsp;
  Loading...
</Button>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div class="spinner-border text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSSpinners;

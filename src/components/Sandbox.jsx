import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import ListGroup from "react-bootstrap/ListGroup";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Accordion from "react-bootstrap/Accordion";
import Badge from "react-bootstrap/Badge";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Container from "react-bootstrap/Container";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class Sandbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <Navbar expand="sm">
          <Container>
            <Navbar.Brand href="#home">
              <img
                src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
                height="32"
                className="d-inline-block align-baseline"
                alt="Airlines Reporting Corporation Logo"
              />
              <div className="navbar-brand-text">Settlement</div>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <NavDropdown title="Dropdown Link" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">
                    Something
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
              <div className="navbar-utility">
                <a href="" className="navbar-utility-link navbar-utility-icon">
                  <i className="fds-button__icon fds-glyphs-bell2"></i>
                </a>
                <a
                  href="#"
                  className="navbar-utility-link navbar-utility-username"
                >
                  User Name
                </a>
                <a href="#" className="navbar-utility-link">
                  Help
                </a>
                <a href="#" className="navbar-utility-link">
                  Close
                </a>
              </div>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <div className="container my-3">
          <div className="row">
            <div className="col-md-12">
              <InputGroup>
                <InputGroup.Prepend>
                  <InputGroup.Text>First and last name</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl />
                <FormControl />
              </InputGroup>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">First and last name</span>
                  </div>
                  <input
                    type="text"
                    aria-label="First name"
                    class="form-control"
                  />
                  <input
                    type="text"
                    aria-label="Last name"
                    class="form-control"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="container my-5">
          <div className="row">
            <div className="col-md-4">
              <Card className="text-left">
                <Card.Header className="card-header-gray">Ticket</Card.Header>
                <Card.Body>
                  <Card.Title>
                    <h1 className="type-heading-h1">123 456789087-48</h1>
                  </Card.Title>
                  <Card.Text as="div">
                    <div>
                      <i className="fds-glyphs-briefcase"></i> John Doe Travel
                      Agency (01234567)
                    </div>
                    <div>
                      <i className="fds-glyphs-airplane"></i> Jane Doe
                      Airline(JD/123)
                    </div>
                  </Card.Text>
                  <p>
                    <Badge variant="warning">Partial Exchange (4,5)</Badge>
                  </p>
                </Card.Body>
              </Card>
            </div>
            <div className="col-md-8">
              <div className="mb-5">
                <div className="row mb-5">
                  <div className="col-md-4">
                    <div className="type-body-secondary-on-light">
                      Passenger Name
                    </div>
                    <div>Rothstein/Richard H</div>
                  </div>
                  <div className="col-md-4">
                    <div className="type-body-secondary-on-light">
                      Record Locator
                    </div>
                    <div>WXACKF/AA</div>
                  </div>
                  <div className="col-md-4">
                    <div className="type-body-secondary-on-light">
                      Frequent Flyer
                    </div>
                    <div>&mdash;</div>
                  </div>
                </div>

                <h2 className="type-heading-h2">Financial Information</h2>
                <div className="mt-3 mb-5">
                  <Accordion className="nested-accordion">
                    <Card className="nested-accordion-card">
                      <Accordion.Toggle
                        className="nested-accordion-card-header icon-left"
                        as={Card.Header}
                        eventKey="0"
                      >
                        <div>
                          <div className="row align-items-center">
                            <div className="col-md-6">
                              <h2 className="type-heading-h2">
                                Original Ticket (This Document)
                              </h2>
                            </div>
                            <div className="col-md-3 text-md-right">
                              <div className="type-caption-on-light">
                                Commision
                              </div>
                              <div>$0.00</div>
                            </div>
                            <div className="col-md-3 text-md-right">
                              <div className="type-caption-on-light">Total</div>
                              <div>$12,005.23</div>
                            </div>
                          </div>
                        </div>
                      </Accordion.Toggle>
                      <Accordion.Collapse
                        className="nested-accordion-collapse"
                        eventKey="0"
                      >
                        <Card.Body>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-none"
                                as={Card.Header}
                              >
                                <div className="row">
                                  <div className="col-md-6">Base Fare</div>
                                  <div className="col-md-6 text-md-right">
                                    $13139.73
                                  </div>
                                </div>
                              </Accordion.Toggle>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-left"
                                as={Card.Header}
                                eventKey="0"
                              >
                                <div className="row">
                                  <div className="col-md-6">Taxes</div>
                                  <div className="col-md-6 text-md-right">
                                    $1079.32
                                  </div>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  Lorem ipsum dolor sit amet consectetur,
                                  adipisicing elit. Fugit minus vero, cupiditate
                                  dolorem minima asperiores esse at! Odio
                                  facilis quidem nulla exercitationem doloribus
                                  et eaque ad earum minima, quis voluptates.
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-left"
                                as={Card.Header}
                                eventKey="0"
                              >
                                <div className="row">
                                  <div className="col-md-6">
                                    Passenger Facility Charges
                                  </div>
                                  <div className="col-md-6 text-md-right">
                                    $1079.32
                                  </div>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  Lorem ipsum dolor sit amet consectetur,
                                  adipisicing elit. Fugit minus vero, cupiditate
                                  dolorem minima asperiores esse at! Odio
                                  facilis quidem nulla exercitationem doloribus
                                  et eaque ad earum minima, quis voluptates.
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-none"
                                as={Card.Header}
                              >
                                <div className="row">
                                  <div className="col-md-6">Document Total</div>
                                  <div className="col-md-6 text-md-right">
                                    $12,060.73
                                  </div>
                                </div>
                              </Accordion.Toggle>
                            </Card>
                          </Accordion>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                  <Accordion className="nested-accordion">
                    <Card className="nested-accordion-card">
                      <Accordion.Toggle
                        className="nested-accordion-card-header icon-left"
                        as={Card.Header}
                        eventKey="0"
                      >
                        <div>
                          <div className="row align-items-center">
                            <div className="col-md-6">
                              <h2 className="type-heading-h2">
                                New Ticket (123 789048940)
                              </h2>
                            </div>
                            <div className="col-md-3 text-md-right">
                              <div className="type-caption-on-light">
                                Commision
                              </div>
                              <div>$0.00</div>
                            </div>
                            <div className="col-md-3 text-md-right">
                              <div className="type-caption-on-light">Total</div>
                              <div>$12,005.23</div>
                            </div>
                          </div>
                        </div>
                      </Accordion.Toggle>
                      <Accordion.Collapse
                        className="nested-accordion-collapse"
                        eventKey="0"
                      >
                        <Card.Body>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-none"
                                as={Card.Header}
                              >
                                <div className="row">
                                  <div className="col-md-6">Base Fare</div>
                                  <div className="col-md-6 text-md-right">
                                    $13139.73
                                  </div>
                                </div>
                              </Accordion.Toggle>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-left"
                                as={Card.Header}
                                eventKey="0"
                              >
                                <div className="row">
                                  <div className="col-md-6">Taxes</div>
                                  <div className="col-md-6 text-md-right">
                                    $1079.32
                                  </div>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  Lorem ipsum dolor sit amet consectetur,
                                  adipisicing elit. Fugit minus vero, cupiditate
                                  dolorem minima asperiores esse at! Odio
                                  facilis quidem nulla exercitationem doloribus
                                  et eaque ad earum minima, quis voluptates.
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-left"
                                as={Card.Header}
                                eventKey="0"
                              >
                                <div className="row">
                                  <div className="col-md-6">
                                    Passenger Facility Charges
                                  </div>
                                  <div className="col-md-6 text-md-right">
                                    $1079.32
                                  </div>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  Lorem ipsum dolor sit amet consectetur,
                                  adipisicing elit. Fugit minus vero, cupiditate
                                  dolorem minima asperiores esse at! Odio
                                  facilis quidem nulla exercitationem doloribus
                                  et eaque ad earum minima, quis voluptates.
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-none"
                                as={Card.Header}
                              >
                                <div className="row">
                                  <div className="col-md-6">Document Total</div>
                                  <div className="col-md-6 text-md-right">
                                    $12,060.73
                                  </div>
                                </div>
                              </Accordion.Toggle>
                            </Card>
                          </Accordion>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                  <Accordion className="nested-accordion">
                    <Card className="nested-accordion-card">
                      <Accordion.Toggle
                        className="nested-accordion-card-header icon-left"
                        as={Card.Header}
                        eventKey="0"
                      >
                        <div>
                          <div className="row align-items-center">
                            <div className="col-md-6">
                              <h2 className="type-heading-h2">
                                Refund wih Exchange
                              </h2>
                            </div>
                            <div className="col-md-3 text-md-right">
                              <div className="type-caption-on-light">
                                Commision
                              </div>
                              <div>$0.00</div>
                            </div>
                            <div className="col-md-3 text-md-right">
                              <div className="type-caption-on-light">Total</div>
                              <div>$12,005.23</div>
                            </div>
                          </div>
                        </div>
                      </Accordion.Toggle>
                      <Accordion.Collapse
                        className="nested-accordion-collapse"
                        eventKey="0"
                      >
                        <Card.Body>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-none"
                                as={Card.Header}
                              >
                                <div className="row">
                                  <div className="col-md-6">Base Fare</div>
                                  <div className="col-md-6 text-md-right">
                                    $13139.73
                                  </div>
                                </div>
                              </Accordion.Toggle>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-left"
                                as={Card.Header}
                                eventKey="0"
                              >
                                <div className="row">
                                  <div className="col-md-6">Taxes</div>
                                  <div className="col-md-6 text-md-right">
                                    $1079.32
                                  </div>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  Lorem ipsum dolor sit amet consectetur,
                                  adipisicing elit. Fugit minus vero, cupiditate
                                  dolorem minima asperiores esse at! Odio
                                  facilis quidem nulla exercitationem doloribus
                                  et eaque ad earum minima, quis voluptates.
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-left"
                                as={Card.Header}
                                eventKey="0"
                              >
                                <div className="row">
                                  <div className="col-md-6">
                                    Passenger Facility Charges
                                  </div>
                                  <div className="col-md-6 text-md-right">
                                    $1079.32
                                  </div>
                                </div>
                              </Accordion.Toggle>
                              <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                  Lorem ipsum dolor sit amet consectetur,
                                  adipisicing elit. Fugit minus vero, cupiditate
                                  dolorem minima asperiores esse at! Odio
                                  facilis quidem nulla exercitationem doloribus
                                  et eaque ad earum minima, quis voluptates.
                                </Card.Body>
                              </Accordion.Collapse>
                            </Card>
                          </Accordion>
                          <Accordion>
                            <Card>
                              <Accordion.Toggle
                                className="icon-none"
                                as={Card.Header}
                              >
                                <div className="row">
                                  <div className="col-md-6">Document Total</div>
                                  <div className="col-md-6 text-md-right">
                                    $12,060.73
                                  </div>
                                </div>
                              </Accordion.Toggle>
                            </Card>
                          </Accordion>
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </div>

                <h2 class="type-heading-h2">Itinerary</h2>
                <ListGroup className="listgroup-itinerary">
                  <ListGroup.Item className="">
                    <div className="list-group-append">1</div>
                    <div className="list-group-content">
                      <div className="row">
                        <div className="col-sm-4">
                          <strong>Aus &raquo; ORD AA129</strong>
                        </div>
                        <div className="col-sm-4">Stopover: O</div>
                        <div className="col-sm-4">Fare Basis: N7AIZNM3</div>
                      </div>
                      <div className="row">
                        <div className="col-sm-4">
                          Departs 10/14/19 at 3:05AM
                        </div>
                        <div className="col-sm-4">Class: N</div>
                        <div className="col-sm-4">Designator: CWT3</div>
                      </div>
                    </div>
                  </ListGroup.Item>
                  <ListGroup.Item className="">
                    <div className="list-group-append">2</div>
                    <div className="list-group-content">
                      <div className="row">
                        <div className="col-sm-4">
                          <strong>Aus &raquo; ORD AA129</strong>
                        </div>
                        <div className="col-sm-4">Stopover: O</div>
                        <div className="col-sm-4">Fare Basis: N7AIZNM3</div>
                      </div>
                      <div className="row">
                        <div className="col-sm-4">
                          Departs 10/14/19 at 3:05AM
                        </div>
                        <div className="col-sm-4">Class: N</div>
                        <div className="col-sm-4">Designator: CWT3</div>
                      </div>
                    </div>
                  </ListGroup.Item>
                  <ListGroup.Item className="listgroup-void">
                    <div className="list-group-append">3</div>
                    <div className="list-group-content">
                      <div className="row">
                        <div className="col-sm-12">
                          <strong>Voided</strong>
                        </div>
                      </div>
                    </div>
                  </ListGroup.Item>
                  <ListGroup.Item className="listgroup-attention">
                    <div className="list-group-append">4</div>
                    <div className="list-group-content">
                      <div className="type-body-attention">Exchanged</div>
                      <div className="row">
                        <div className="col-sm-4">
                          <strong>Aus &raquo; ORD AA129</strong>
                        </div>
                        <div className="col-sm-4">Stopover: O</div>
                        <div className="col-sm-4">Fare Basis: N7AIZNM3</div>
                      </div>
                      <div className="row">
                        <div className="col-sm-4">
                          Departs 10/14/19 at 3:05AM
                        </div>
                        <div className="col-sm-4">Class: N</div>
                        <div className="col-sm-4">Designator: CWT3</div>
                      </div>
                    </div>
                  </ListGroup.Item>
                  <ListGroup.Item className="listgroup-attention">
                    <div className="list-group-append">5</div>
                    <div className="list-group-content">
                      <div className="type-body-attention">Exchanged</div>
                      <div className="row">
                        <div className="col-sm-4">
                          <strong>Aus &raquo; ORD AA129</strong>
                        </div>
                        <div className="col-sm-4">Stopover: O</div>
                        <div className="col-sm-4">Fare Basis: N7AIZNM3</div>
                      </div>
                      <div className="row">
                        <div className="col-sm-4">
                          Departs 10/14/19 at 3:05AM
                        </div>
                        <div className="col-sm-4">Class: N</div>
                        <div className="col-sm-4">Designator: CWT3</div>
                      </div>
                    </div>
                  </ListGroup.Item>
                </ListGroup>
              </div>

              <Form>
                <Form.Check
                  type="switch"
                  id="custom-switch-this"
                  label="Check this switch"
                  className="custom-switch-on-off"
                />

                <br />

                <Form.Check
                  type="switch"
                  id="custom-switch-that"
                  label="Check this switch"
                />

                <br />

                <Form.Check
                  disabled
                  type="switch"
                  label="disabled switch"
                  id="disabled-custom-switch"
                />

                <br />

                <Form.Check
                  defaultChecked
                  disabled
                  type="switch"
                  label="disabled switch"
                  id="disabled-custom-switch"
                />
              </Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Sandbox;

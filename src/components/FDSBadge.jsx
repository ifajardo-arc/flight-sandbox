import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Badge from "react-bootstrap/Badge";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSBadge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Badge</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    The badge component is used to call out a numerical value.
                    They can be paied with a 24px FDS glyph or used inline.
                    There are three badge types defined.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <div>
                    <Badge pill variant="primary">
                      5
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="primary">
                      55
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="primary">
                      555
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="success">
                      5
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="success">
                      55
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="success">
                      555
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="danger">
                      5
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="danger">
                      55
                    </Badge>{" "}
                    &nbsp;
                    <Badge pill variant="danger">
                      555
                    </Badge>{" "}
                    &nbsp;
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Badge from "react-bootstrap/Badge";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Badge pill variant="primary">5</Badge> 

<Badge pill variant="primary">55</Badge> 

<Badge pill variant="primary">555</Badge>  `}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<span class="badge badge-pill badge-primary">5</span>

<span class="badge badge-pill badge-primary">55</span> 

<span class="badge badge-pill badge-primary">555</span> 
`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSBadge;

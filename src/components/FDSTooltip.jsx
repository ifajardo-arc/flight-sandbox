import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSTooltip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    const tooltip = (
      <Tooltip>
        Tooltip message
      </Tooltip>
    );

    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Tooltips</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Tooltips are a convenient way to presenting additiona
                    information to your user. They are tiny little clouds with a
                    brief text message triggered by clicking on specified
                    element or hovering over it.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <OverlayTrigger
                    placement="left"
                    overlay={tooltip}
                  >
                    <Button variant="primary">Tooltip on left</Button>
                  </OverlayTrigger> &nbsp;
                  <OverlayTrigger
                    placement="bottom"
                    overlay={tooltip}
                  >
                    <Button variant="primary">Tooltip on bottom</Button>
                  </OverlayTrigger> &nbsp;
                  <OverlayTrigger
                    placement="top"
                    overlay={tooltip}
                  >
                    <Button variant="primary">Tooltip on top</Button>
                  </OverlayTrigger> &nbsp;
                  <OverlayTrigger
                    placement="right"
                    overlay={tooltip}
                  >
                    <Button variant="primary">Tooltip on right</Button>
                  </OverlayTrigger>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Tabs defaultActiveKey="react" id="link-example">
                    <Tab eventKey="react" title="React">
                      <pre>
                        <code className="language-jsx">
                          {`<OverlayTrigger
  placement="left"
  overlay={
    <Tooltip>
      Tooltip message
    </Tooltip>
  } >
  <Button variant="primary">Tooltip on left</Button>
</OverlayTrigger>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSTooltip;

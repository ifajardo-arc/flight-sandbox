import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Buttons</h1>
                  <p className="type-heading-h3-secondary">
                    Custom button styles for actions in forms, dialogs, and more
                    with support for multiple sizes, states, and more.
                  </p>
                </div>
              </div>
              <div className="row pt-3">
                <div className="col-md-6">
                  <strong>Primary Button</strong>
                  <p className="type-body-primary-on-light">
                    Primary buttons should be used for the main call to action
                    item. It’s best practice to limit these to one per page when
                    possible. There are two types of primary buttons: text and
                    text with icon.
                  </p>
                  <div className="mt-3">
                    <Button variant="primary">Click Here</Button>
                  </div>
                </div>
                <div className="col-md-6">
                  <strong>Affirmation Button</strong>
                  <p className="type-body-primary-on-light">
                    Affirmation buttons should be used when the user is saving
                    new information or completing a multi-step process. These
                    should be limited to one per page and clearly communicate a
                    saved or completed action. There are two types of
                    affirmation buttons: text and text with icon. When words are
                    not enough icons can be used to help communicate the
                    intended action. Icons should always be to the right of the
                    button text and the same color as the button text.
                  </p>
                  <div className="mt-3">
                    <Button variant="success">Click Here</Button>
                  </div>
                </div>
                <div className="col-md-6">
                  <strong>Destructive Button</strong>
                  <div className="mt-3">
                    <Button variant="danger">Click Here</Button>
                  </div>
                </div>
              </div>
              <div className="row pt-3">
                <div className="col-md-6">
                  <strong>Secondary Button</strong>
                  <p className="type-body-primary-on-light">
                    Secondary buttons are typically used in close proximity to
                    primary buttons and offen provide an alternate action to the
                    primary. It’s best practice to limit these to one per page
                    when possible. There are two types of secondary buttons:
                    text and text with icon.
                  </p>
                  <div className="mt-3">
                    <Button variant="secondary">Click Here</Button>
                  </div>
                </div>
                <div className="col-md-6">
                  <strong>Ghost Button</strong>
                  <p className="type-body-primary-on-light">
                    Ghost buttons are similar to basic text links but behave
                    like buttons. They should not be used inline with content.
                    Use text links for those needs.
                  </p>
                  <div className="mt-3">
                    <Button variant="link">Click Here</Button>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Button from "react-bootstrap/Button";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Button variant="primary">Click Here</Button>
<Button variant="success">Click Here</Button>
<Button variant="danger">Click Here</Button>
<Button variant="secondary">Click Here</Button>
<Button variant="link">Click Here</Button>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<button className="btn btn-primary">Learn More</button>
<button className="btn btn-success">Learn More</button>
<button className="btn btn-danger">Learn More</button>
<button className="btn btn-secondary">Learn More</button>
<button className="btn btn-link">Learn More</button>
`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
              <div className="row">
                <div className="col-md-3">
                  <button className="btn btn-primary btn-lg">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-primary">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-primary btn-sm">Learn More</button>
                </div>
                <div className="col-md-9">
                  <pre>
                    <code className="language-html">
                      {`<button className="btn btn-primary">Learn More</button>`}
                    </code>
                  </pre>
                  <pre>
                    <code className="language-html">
                      {`<button className="btn btn-primary btn-md">Learn More</button>`}
                    </code>
                  </pre>
                  <pre>
                    <code className="language-html">
                      {`<button className="btn btn-primary btn-sm">Learn More</button>`}
                    </code>
                  </pre>
                </div>
              </div>
              <div className="row">
                <div className="col-md-3">
                  <p className="type-body-primary-on-light">
                    <code className="language-css">{`.btn .btn-primary`}</code>
                  </p>

                  <button className="btn btn-primary btn-lg">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-primary ">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-primary btn-sm">Learn More</button>
                </div>
                <div className="col-md-3">
                  <p className="type-body-primary-on-light">
                    <code className="language-css">{`.btn .btn-success`}</code>
                  </p>
                  <button className="btn btn-success btn-lg">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-success">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-success btn-sm">Learn More</button>
                </div>
                <div className="col-md-3">
                  <p className="type-body-primary-on-light">
                    <code className="language-css">{`.btn .btn-secondary`}</code>
                  </p>
                  <button className="btn btn-secondary btn-lg">
                    Learn More
                  </button>
                  <br />
                  <br />
                  <button className="btn btn-secondary">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-secondary btn-sm">
                    Learn More
                  </button>
                </div>
                <div className="col-md-3">
                  <p className="type-body-primary-on-light">
                    <code className="language-css">{`.btn .btn-link`}</code>
                  </p>
                  <button className="btn btn-link btn-lg">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-link">Learn More</button>
                  <br />
                  <br />
                  <button className="btn btn-link btn-sm">Learn More</button>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />  
          </div> 
        </div>
      </div>
    );
  }
}

export default FDSButton;

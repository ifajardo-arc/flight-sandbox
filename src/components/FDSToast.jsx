import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Toast from "react-bootstrap/Toast";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSToast extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toastDisplay: true
    };

    this.toggleToast = this.toggleToast.bind(this);
  }

  toggleToast() {
    if(this.state.toastDisplay === false){
      this.setState({toastDisplay: true});
    }
    else {
      this.setState({toastDisplay: false});
    }
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Toast</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Toasts are lightweight notifications designed to mimic the
                    push notifications that have been popularized by mobile and
                    desktop operating systems. They’re built with flexbox, so
                    they’re easy to align and position.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <Toast show={this.state.toastDisplay} onClose={this.toggleToast}>
                    <Toast.Header>
                      <strong className="mr-auto">Bootstrap</strong>
                      <small>11 mins ago</small>
                    </Toast.Header>
                    <Toast.Body>
                      Hello, world! This is a toast message.
                    </Toast.Body>
                  </Toast>

                  <button onClick={this.toggleToast} className="btn btn-primary">Toggle Toast</button> 
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Card from "react-bootstrap/Card";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Toast show={this.state.toastDisplay} onClose={this.toggleToast}>
  <Toast.Header>
    <strong className="mr-auto"> FDS Bootstrap</strong>
    <small>11 mins ago</small>
  </Toast.Header>
  <Toast.Body>
    Hello, world! This is a toast message.
  </Toast.Body>
</Toast>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="toast-header">
    <strong class="mr-auto">FDS Bootstrap</strong>
    <small>11 mins ago</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="toast-body">
    Hello, world! This is a toast message.
  </div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSToast;

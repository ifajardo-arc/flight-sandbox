import React, { Component } from "react";
import { HashRouter as Router, Route, Link } from "react-router-dom";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";

class Bootstrapnav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.setloc = this.setloc.bind(this);
  }

  setloc(pathname) {
    if (pathname) {
      if (pathname.indexOf("components") > -1) {
        return "1";
      } else if (pathname.indexOf("getting-started") > -1) {
        return "0";
      }
    }

    return "0";
  }

  render() {
    return (
      <div className="bootstrap-sidebar">
        <img
          src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
          alt=""
          height="30px"
          className="mb-3"
        />

        <h2 className="type-heading-h2-on-dark">FDS Bootstrap</h2>
        {this.props.set}
        <Accordion defaultActiveKey={this.setloc(this.props.loc)}>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="0">
              Getting Started
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <Card.Body>
                <Link className="bootstrap-sidebar-link" to="/">
                  Introduction
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/getting-started/tokens"
                >
                  Design Tokens
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/getting-started/grid"
                >
                  Grid
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/getting-started/typography"
                >
                  Typography
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/getting-started/headerfooter"
                >
                  Header & Footer
                </Link>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="1">
              Components
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="1">
              <Card.Body>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/accordion"
                >
                  Accordion
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/badge">
                  Badge
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/breadcrumb"
                >
                  Breadcrumb
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/button"
                >
                  Button
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/card">
                  Card
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/datepicker"
                >
                  Datepicker
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/dropdown"
                >
                  Dropdown
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/form">
                  Forms
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/inputgroup">
                  Input Group
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/link">
                  Link
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/listgroup"
                >
                  List Group
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/modal">
                  Modal
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/overflow"
                >
                  Overflow Menu
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/popovers"
                >
                  Popovers
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/table">
                  Table
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/tag">
                  Tag
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/toast">
                  Toast
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/tooltips"
                >
                  Tooltips
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/pagination"
                >
                  Pagination
                </Link>
                <Link
                  className="bootstrap-sidebar-link"
                  to="/components/progressbar"
                >
                  Progress Bar
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/spinners">
                  Spinners
                </Link>
                <Link className="bootstrap-sidebar-link" to="/components/tabs">
                  Tabs
                </Link>
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
    );
  }
}

export default Bootstrapnav;

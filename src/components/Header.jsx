import React, { Component } from "react";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };

    this.increment = this.increment.bind(this);
  }

  componentDidMount() {}

  increment() {
    this.setState((state) => ({
      value: state.value + 1
    }));
  }

  render() {
    return (
      <div>
        <h1 style={{ color: this.props.color }}>{this.props.title}</h1>
        <div style={{ color: this.props.jobcolor }}>{this.props.jobtitle}</div>
        <div>{this.props.jobdescription}</div>
        <div>{this.state.value}</div>
        <button onClick={this.increment}>Increment</button>
      </div>
    );
  }
}

export default Header;

import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Dropdown from "react-bootstrap/Dropdown";
import ButtonGroup from "react-bootstrap/ButtonGroup";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Dropdown</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    The Dropdown component gives users a list of options that
                    can be used to filter or sort existing data. They can also
                    be used as menus. Currently there are two types of dropdowns
                    defined – Default and Leading icon. In the future, more
                    types may be needed as components are added to Flight.{" "}
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Dropdown className="fds-dropdown">
                    <Dropdown.Toggle id="dropdown-basic">
                      Dropdown Button
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/components/dropdown">
                        Action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/components/dropdown">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/components/dropdown">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>

                  <br/><br/>

                  <Dropdown as={ButtonGroup}>
                    <Button variant="success">Split Button</Button>

                    <Dropdown.Toggle
                      split
                      variant="success"
                      id="dropdown-split-basic"
                    />

                    <Dropdown.Menu>
                      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                      <Dropdown.Item href="#/action-2">
                        Another action
                      </Dropdown.Item>
                      <Dropdown.Item href="#/action-3">
                        Something else
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Dropdown from "react-bootstrap/Dropdown";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Dropdown className="fds-dropdown">
  <Dropdown.Toggle id="dropdown-basic">
    Dropdown Button
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item href="#/components/dropdown">
      Action
    </Dropdown.Item>
    <Dropdown.Item href="#/components/dropdown">
      Another action
    </Dropdown.Item>
    <Dropdown.Item href="#/components/dropdown">
      Something else
    </Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>

<br/><br/>

<Dropdown as={ButtonGroup}>
  <Button variant="success">Split Button</Button>

  <Dropdown.Toggle
    split
    variant="success"
    id="dropdown-split-basic"
  />

  <Dropdown.Menu>
    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
    <Dropdown.Item href="#/action-2">
      Another action
    </Dropdown.Item>
    <Dropdown.Item href="#/action-3">
      Something else
    </Dropdown.Item>
  </Dropdown.Menu>
</Dropdown>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div class="dropdown">
  <a
    class="btn btn-primary dropdown-toggle" 
    href="#"
    role="button"
    id="dropdownMenuLink"
    data-toggle="dropdown"
    aria-haspopup="true"
    aria-expanded="false"
  >
    Dropdown link
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="#">Action</a>
    <a class="dropdown-item" href="#">Another action</a>
    <a class="dropdown-item" href="#">Something else here</a>
  </div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSDropdown;

import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSPopover extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    const popover = (
      <Popover id="popover-basic">
        <Popover.Title as="h3">Popover Title</Popover.Title>
        <Popover.Content>
          This is a popover body content. Popovers are typically dismissable,
          whether by click on other parts or second clicking the popover target.
        </Popover.Content>
      </Popover>
    );

    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Popovers</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    A popover is used to embed a longer note into a tag; by
                    default they are activated by clickon the tag. A tooltip
                    only has a text title; a popover has a title, which is
                    generally text, and content, which can be HTML.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <OverlayTrigger
                    trigger="click"
                    placement="left"
                    overlay={popover}
                    rootClose={true}
                    rootCloseEvent="click"
                  >
                    <Button variant="primary">Click me to see</Button>
                  </OverlayTrigger>{" "}
                  &nbsp;
                  <OverlayTrigger
                    trigger="click"
                    placement="bottom"
                    overlay={popover}
                    rootClose={true}
                    rootCloseEvent="click"
                  >
                    <Button variant="primary">Click me to see</Button>
                  </OverlayTrigger>{" "}
                  &nbsp;
                  <OverlayTrigger
                    trigger="click"
                    placement="top"
                    overlay={popover}
                    rootClose={true}
                    rootCloseEvent="click"
                  >
                    <Button variant="primary">Click me to see</Button>
                  </OverlayTrigger>{" "}
                  &nbsp;
                  <OverlayTrigger
                    trigger="click"
                    placement="right"
                    overlay={popover}
                    rootClose={true}
                    rootCloseEvent="click"
                  >
                    <Button variant="primary">Click me to see</Button>
                  </OverlayTrigger>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Tabs defaultActiveKey="react" id="link-example">
                    <Tab eventKey="react" title="React">
                      <pre>
                        <code className="language-jsx">
                          {`const popover = (
  <Popover id="popover-basic">
    <Popover.Title as="h3">Popover Title</Popover.Title>
    <Popover.Content>
      This is a popover body content. Popovers are typically dismissable,
      whether by click on other parts or second clicking the popover target.
    </Popover.Content>
  </Popover>
);

const Example = () => (
  <OverlayTrigger
    trigger="click"
    placement="right"
    overlay={popover}
    rootClose={true}
    rootCloseEvent="click"
  >
    <Button variant="primary">Click me to see</Button>
  </OverlayTrigger>
);

render(<Example />);`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSPopover;

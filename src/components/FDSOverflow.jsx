import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSOverflow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4" style={{ height: "100%" }}>
              <div className="row" style={{ height: "100%" }}>
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Overflow Menu</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    The overflow menu can be used when additional actions need
                    to be displayed and there is a space constraint. The icon
                    used to trigger the overflow menu is typically a vertical
                    elipse, but could vary across use cases.
                  </p>
                  <p className="pb-3">
                    Not supported in Bootstrap: refer to FDS documentation. An
                    alternative component can be used using these guideless to
                    style the component as close as possible.
                  </p>
                  <a
                    href="https://flightds.netlify.com/component-blueprints/overflow-menu/blueprint"
                    target="_blank"
                    className="btn btn-primary"
                  >
                    Blueprint
                  </a>{" "}
                  <a
                    href="https://flightds.netlify.com/component-blueprints/overflow-menu/usage"
                    target="_blank"
                    className="btn btn-primary"
                  >
                    Usage
                  </a>{" "}
                  <a
                    href="https://flightds.netlify.com/component-blueprints/overflow-menu/anatomy"
                    target="_blank"
                    className="btn btn-primary"
                  >
                    Anatomy
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSOverflow;

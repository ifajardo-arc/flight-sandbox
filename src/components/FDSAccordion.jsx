import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSAccordion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Accordion</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    The accordion component is used to display large amounts of
                    content when space is limited. They are helpful to use when
                    the underlying content is not important to see right away,
                    and they work especially well on mobile views where
                    additional vertical space is available.
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="offset-md-2 col-md-8">
                  <Accordion>
                    <Card>
                      <Accordion.Toggle as={Card.Header} eventKey="0">
                        Accordion 1
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="0">
                        <Card.Body>
                          Lorem ipsum dolor sit amet consectetur, adipisicing
                          elit. Fugit minus vero, cupiditate dolorem minima
                          asperiores esse at! Odio facilis quidem nulla
                          exercitationem doloribus et eaque ad earum minima,
                          quis voluptates.
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                    <Card>
                      <Accordion.Toggle as={Card.Header} eventKey="1">
                        Accordion 2
                      </Accordion.Toggle>
                      <Accordion.Collapse eventKey="1">
                        <Card.Body>
                          Lorem ipsum dolor sit amet consectetur, adipisicing
                          elit. Fugit minus vero, cupiditate dolorem minima
                          asperiores esse at! Odio facilis quidem nulla
                          exercitationem doloribus et eaque ad earum minima,
                          quis voluptates.
                        </Card.Body>
                      </Accordion.Collapse>
                    </Card>
                  </Accordion>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Accordion from "react-bootstrap/Accordion";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Accordion>
  <Card>
    <Accordion.Toggle as={Card.Header} eventKey="0">
      Accordion 1
    </Accordion.Toggle>
    <Accordion.Collapse eventKey="0">
      <Card.Body>
        Lorem ipsum dolor sit amet consectetur, adipisicing
        elit. Fugit minus vero, cupiditate dolorem minima
        asperiores esse at! Odio facilis quidem nulla
        exercitationem doloribus et eaque ad earum minima,
        quis voluptates.
      </Card.Body>
    </Accordion.Collapse>
  </Card>
  <Card>
    <Accordion.Toggle as={Card.Header} eventKey="1">
      Accordion 2
    </Accordion.Toggle>
    <Accordion.Collapse eventKey="1">
      <Card.Body>
        Lorem ipsum dolor sit amet consectetur, adipisicing
        elit. Fugit minus vero, cupiditate dolorem minima
        asperiores esse at! Odio facilis quidem nulla
        exercitationem doloribus et eaque ad earum minima,
        quis voluptates.
      </Card.Body>
    </Accordion.Collapse>
  </Card>
</Accordion>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<div className="card">
  <div className="card-body">
    <h1 className="type-heading-h1">Card Title</h1>
    <p className="type-body-primary-on-light">Some quick example text to build on the card title and make
      up the bulk of the card's content.</p>
    <Button variant="primary">Click Here</Button>
  </div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSAccordion;

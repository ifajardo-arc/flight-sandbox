import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSInputgroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Input Group</h1>
                  <p className="pb-3 type-heading-h3-secondary">
                    Place one add-on or button on either side of an input. You
                    may also place one on both sides of an input. Remember to
                    place labels outside the input group.
                  </p>
                </div>
              </div>

              <div className="row">
                <div className="col-md-10">
                  <div>
                    <InputGroup className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        placeholder="Username"
                        aria-label="Username"
                        aria-describedby="basic-addon1"
                      />
                    </InputGroup>

                    <InputGroup className="mb-3">
                      <FormControl
                        placeholder="Recipient's username"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                      />
                      <InputGroup.Append>
                        <InputGroup.Text id="basic-addon2">
                          @example.com
                        </InputGroup.Text>
                      </InputGroup.Append>
                    </InputGroup>

                    <label htmlFor="basic-url">Your vanity URL</label>
                    <InputGroup className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon3">
                          https://example.com/users/
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        id="basic-url"
                        aria-describedby="basic-addon3"
                      />
                    </InputGroup>

                    <InputGroup className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text>$</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl aria-label="Amount (to the nearest dollar)" />
                      <InputGroup.Append>
                        <InputGroup.Text>.00</InputGroup.Text>
                      </InputGroup.Append>
                    </InputGroup>

                    <InputGroup>
                      <InputGroup.Prepend>
                        <InputGroup.Text>With textarea</InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl as="textarea" aria-label="With textarea" />
                    </InputGroup>
                  </div>

                  <h2 className="type-heading-h2 mt-5">Sizing</h2>
                  <p>
                    Add the relative form sizing classes to the InputGroup and
                    contents within will automatically resize—no need for
                    repeating the form control size classes on each element.
                  </p>

                  <div className="pt-3">
                    <InputGroup size="sm" className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-sm">
                          Small
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        aria-label="Small"
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </InputGroup>
                    <br />
                    <InputGroup className="mb-3">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">
                          Default
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        aria-label="Default"
                        aria-describedby="inputGroup-sizing-default"
                      />
                    </InputGroup>
                    <br />
                    <InputGroup size="lg">
                      <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-lg">
                          Large
                        </InputGroup.Text>
                      </InputGroup.Prepend>
                      <FormControl
                        aria-label="Large"
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </InputGroup>
                  </div>

                  <h2 className="type-heading-h2 mt-5">Button Addons</h2>

                  <div className="pt-3">
                    <InputGroup className="mb-3">
                      <InputGroup.Prepend>
                        <Button variant="secondary">Button</Button>
                      </InputGroup.Prepend>
                      <FormControl aria-describedby="basic-addon1" />
                    </InputGroup>

                    <InputGroup className="mb-3">
                      <FormControl
                        placeholder="Recipient's username"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                      />
                      <InputGroup.Append>
                        <Button variant="secondary">Button</Button>
                      </InputGroup.Append>
                    </InputGroup>

                    <strong>Small Sizing</strong>

                    <InputGroup size="sm" className="mb-3">
                      <InputGroup.Prepend>
                        <Button variant="secondary" size="sm">
                          Button
                        </Button>
                      </InputGroup.Prepend>
                      <FormControl
                        aria-label="Small"
                        aria-describedby="inputGroup-sizing-sm"
                      />
                    </InputGroup>

                    <InputGroup size="sm" className="mb-3">
                      <FormControl
                        aria-label="Small"
                        aria-describedby="inputGroup-sizing-sm"
                      />
                      <InputGroup.Append>
                        <Button variant="secondary" size="sm">
                          Button
                        </Button>
                      </InputGroup.Append>
                    </InputGroup>
                  </div>

                  <div className="pt-3">
                    <InputGroup className="mb-3">
                      <DropdownButton
                        as={InputGroup.Prepend}
                        title="Dropdown"
                        id="input-group-dropdown-1"
                      >
                        <Dropdown.Item href="#">Action</Dropdown.Item>
                        <Dropdown.Item href="#">Another action</Dropdown.Item>
                        <Dropdown.Item href="#">
                          Something else here
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item href="#">Separated link</Dropdown.Item>
                      </DropdownButton>
                      <FormControl aria-describedby="basic-addon1" />
                    </InputGroup>

                    <InputGroup>
                      <FormControl
                        placeholder="Recipient's username"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                      />

                      <DropdownButton
                        as={InputGroup.Append}
                        title="Dropdown"
                        id="input-group-dropdown-2"
                      >
                        <Dropdown.Item href="#">Action</Dropdown.Item>
                        <Dropdown.Item href="#">Another action</Dropdown.Item>
                        <Dropdown.Item href="#">
                          Something else here
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item href="#">Separated link</Dropdown.Item>
                      </DropdownButton>
                    </InputGroup>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="cards-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <pre>
                        <code className="language-javascript">
                          {`import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";`}
                        </code>
                      </pre>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {` //Text Input
<Form>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email" />
    <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text>
  </Form.Group>

  //Text Input
  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group>

  //Input with button addon
  <InputGroup className="mb-3">
    <FormControl
      placeholder="Recipient's username"
      aria-label="Recipient's username"
      aria-describedby="basic-addon2"
    />
    <InputGroup.Append>
      <Button>
        <i className="fds-button__icon fds-glyphs-search"></i>
      </Button>
    </InputGroup.Append>
  </InputGroup>

  //Checkbox
  <Form.Group controlId="formBasicCheckbox">
    <Form.Check type="checkbox" label="Check me out" />
  </Form.Group>
  <Form.Group controlId="exampleForm.ControlSelect2">
    <Form.Label>Example multiple select</Form.Label>
    <Form.Control as="select" multiple>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </Form.Control>
  </Form.Group>
  <Button variant="primary" type="submit">
    Submit
  </Button>
</Form>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<form>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Example select</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSInputgroup;

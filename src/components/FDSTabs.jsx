import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

class FDSTabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Tabs</h1>
                  <p className="pb-3 type-heading-h3-secondary">The tab component is used to navigate between different views within the same context.</p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <Tabs
                    defaultActiveKey="profile"
                    id="tab-tab-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="Tab 1">
                      Content for Tab 1
                    </Tab>
                    <Tab eventKey="profile" title="Tab 2">
                      Content for Tab 2
                    </Tab>
                    <Tab eventKey="contact" title="Disabled Tab" disabled>
                      Content for Tab 3
                    </Tab>
                  </Tabs>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12 pt-3">
                  <Tabs
                    defaultActiveKey="home"
                    id="tab-code-example"
                    transition={false}
                  >
                    <Tab eventKey="home" title="React">
                      <code className="language-javascript">
                        import Tabs from "react-bootstrap/Tabs";
                      </code>
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-jsx">
                          {`<Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
  <Tab eventKey="home" title="Home">
    Content for Tab 1
  </Tab>
  <Tab eventKey="profile" title="Profile">
    Content for Tab 2
  </Tab>
  <Tab eventKey="contact" title="Contact" disabled>
    Content for Tab 3
  </Tab>
</Tabs>`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre style={{ padding: "2rem" }}>
                        <code className="language-html">
                          {`<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" href="#">Active</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Link</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
  </li>
</ul>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />  
          </div>        
        </div>
      </div>
    );
  }
}

export default FDSTabs;

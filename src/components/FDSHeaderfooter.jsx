import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Container from "react-bootstrap/Container";

class FDSHeaderfooter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div>
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav loc={this.props.location.pathname} />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-4">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1 pt-3">Header & Footer</h1>
                  <p>
                   &nbsp;
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Navbar expand="lg">
                    <Container>
                      <Navbar.Brand href="#home">
                        <img
                          src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
                          width="65"
                          height="24"
                          className="d-inline-block align-baseline"
                          alt="Airlines Reporting Corporation Logo"
                        />
                        <div class="navbar-brand-text">React-Bootstrap</div>
                      </Navbar.Brand>
                      <Navbar.Toggle aria-controls="basic-navbar-nav" />
                      <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                          <Nav.Link href="#home">Home</Nav.Link>
                          <Nav.Link href="#link">Link</Nav.Link>
                          <Nav.Link href="#link">Link</Nav.Link>
                          <NavDropdown
                            title="Dropdown Link"
                            id="basic-nav-dropdown"
                          >
                            <NavDropdown.Item href="#action/3.1">
                              Action
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">
                              Another action
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">
                              Something
                            </NavDropdown.Item>
                          </NavDropdown>
                        </Nav>
                        <div className="navbar-utility">
                          <a
                            href=""
                            className="navbar-utility-link navbar-utility-icon"
                          >
                            <i className="fds-button__icon fds-glyphs-bell2"></i>
                          </a>
                          <a
                            href="#"
                            className="navbar-utility-link navbar-utility-username"
                          >
                            User Name
                          </a>
                          <a href="#" className="navbar-utility-link">
                            Help
                          </a>
                          <a href="#" className="navbar-utility-link">
                            Close
                          </a>
                        </div>
                      </Navbar.Collapse>
                    </Container>
                  </Navbar>
                  <div style={{ background: "#ddd", padding: "100px 0", textAlign: "center" }}>
                    Page Content
                  </div>
                  <div className="footer">
                    <div className="container">
                      <div className="row">
                        <div className="col-lg-7">
                          <p>&copy; 2019 Airlines Reporting Corporation</p>
                          <p>
                            SENSITIVE AND CONFIDENTIAL. Data presented or
                            downloaded here may contain ARC-sensitive
                            information. Disclosure to 3rd parties is prohibited
                            without prior written consent of ARC.
                          </p>
                        </div>
                        <div className="col-lg-5">
                          <div className="footer-links">
                            <a className="fds-link" href="#">
                              Terms Of Use
                            </a>
                            <a className="fds-link" href="#">
                              Privacy Policy
                            </a>
                            <a className="fds-link" href="#">
                              Contact Us
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <Tabs defaultActiveKey="react" id="link-example">
                    <Tab eventKey="react" title="React">
                      <pre>
                        <code className="language-jsx">
                          {`<!-- header -->
<Navbar expand="sm">
  <Container>
    <Navbar.Brand href="#home">
      <img
        src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
        width="65"
        height="24"
        className="d-inline-block align-baseline"
        alt="Airlines Reporting Corporation Logo"
      />
      <div class="navbar-brand-text">React-Bootstrap</div>
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <Nav.Link href="#home">Home</Nav.Link>
        <Nav.Link href="#link">Link</Nav.Link>
        <Nav.Link href="#link">Link</Nav.Link>
        <NavDropdown
          title="Dropdown Link"
          id="basic-nav-dropdown"
        >
          <NavDropdown.Item href="#action/3.1">
            Action
          </NavDropdown.Item>
          <NavDropdown.Item href="#action/3.2">
            Another action
          </NavDropdown.Item>
          <NavDropdown.Item href="#action/3.3">
            Something
          </NavDropdown.Item>
        </NavDropdown>
      </Nav>
      <div className="navbar-utility">
        <a
          href=""
          className="navbar-utility-link navbar-utility-icon"
        >
          <i className="fds-button__icon fds-glyphs-bell2"></i>
        </a>
        <a
          href="#"
          className="navbar-utility-link navbar-utility-username"
        >
          User Name
        </a>
        <a href="#" className="navbar-utility-link">
          Help
        </a>
        <a href="#" className="navbar-utility-link">
          Close
        </a>
      </div>
    </Navbar.Collapse>
  </Container>
</Navbar>

<!-- footer -->
<div className="footer">
  <div className="container">
    <div className="row">
      <div className="col-lg-7">
        <p>&copy; 2019 Airlines Reporting Corporation</p>
        <p>
          SENSITIVE AND CONFIDENTIAL. Data presented or
          downloaded here may contain ARC-sensitive
          information. Disclosure to 3rd parties is prohibited
          without prior written consent of ARC.
        </p>
      </div>
      <div className="col-lg-5">
        <div className="footer-links">
          <a className="fds-link" href="#">
            Terms Of Use
          </a>
          <a className="fds-link" href="#">
            Privacy Policy
          </a>
          <a className="fds-link" href="#">
            Contact Us
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="html" title="HTML">
                      <pre>
                        <code className="language-html">
                          {`<!-- header -->
<div class="navbar navbar-expand-lg navbar-light">
  <div class="container">
    <a href="#home" class="navbar-brand"><img src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png" width="65" height="24" class="d-inline-block align-baseline" alt="Airlines Reporting Corporation Logo">React-Bootstrap</a>
    <button aria-controls="basic-navbar-nav" type="button" aria-label="Toggle navigation" class="navbar-toggler collapsed">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="basic-navbar-nav">
        <div class="mr-auto navbar-nav">
          <a href="#home" data-rb-event-key="#home" class="nav-link">Home</a>
          <a href="#link" data-rb-event-key="#link" class="nav-link">Link</a>
          <a href="#link" data-rb-event-key="#link" class="nav-link">Link</a>
          <div class="dropdown nav-item">
              <a aria-haspopup="true" aria-expanded="false" href="#" class="dropdown-toggle nav-link" role="button">Dropdown Link</a>
              <div class="dropdown-menu" aria-labelledby="">
                <a href="#action/3.1" class="dropdown-item">Action</a>
                <a href="#action/3.2" class="dropdown-item">Another action</a>
                <a href="#action/3.3" class="dropdown-item">Something</a>
              </div>
          </div>
        </div>
        <div class="navbar-utility">
          <a href="" class="navbar-utility-link navbar-utility-icon">
            <i class="fds-button__icon fds-glyphs-bell2"></i>
          </a>
          <a href="#" class="navbar-utility-link navbar-utility-username">User Name</a>
          <a href="#" class="navbar-utility-link">Help</a>
          <a href="#" class="navbar-utility-link">Close</a>
        </div>
    </div>
  </div>
</div>

<!-- footer -->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
        <p>&copy; 2019 Airlines Reporting Corporation</p>
        <p>
          SENSITIVE AND CONFIDENTIAL. Data presented or
          downloaded here may contain ARC-sensitive
          information. Disclosure to 3rd parties is prohibited
          without prior written consent of ARC.
        </p>
      </div>
      <div class="col-lg-5">
        <div class="footer-links">
          <a class="fds-link" href="#">
            Terms Of Use
          </a>
          <a class="fds-link" href="#">
            Privacy Policy
          </a>
          <a class="fds-link" href="#">
            Contact Us
          </a>
        </div>
      </div>
    </div>
  </div>
</div>`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default FDSHeaderfooter;

import "react-app-polyfill/ie11";
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { hot } from "react-hot-loader";

//pages
import Homepage from "./pages/Homepage";
import Routetest from "./pages/Routetest";
import Fdsbootstrap from "./pages/Fdsbootstrap";
import Layout from "./pages/Layout";
import Setup from "./pages/Setup";
import Lunchandlearn from "./pages/Lunchandlearn";

import FDSTypography from "./components/FDSTypography";
import FDSTokens from "./components/FDSTokens";

//components pages
import FDSAccordion from "./components/FDSAccordion";
import FDSBreadcrumb from "./components/FDSBreadcrumb";
import FDSButton from "./components/FDSButton";
import FDSCard from "./components/FDSCard";
import FDSBadge from "./components/FDSBadge";
import FDSLink from "./components/FDSLink";
import FDSDropdown from "./components/FDSDropdown";
import FDSPagination from "./components/FDSPagination";
import FDSTable from "./components/FDSTable";
import FDSTabs from "./components/FDSTabs";
import FDSForms from "./components/FDSForms";
import FDSOverflow from "./components/FDSOverflow";
import FDSDatepicker from "./components/FDSDatepicker";
import FDSTag from "./components/FDSTag";
import FDSGrid from "./components/FDSGrid";
import FDSToast from "./components/FDSToast";
import FDSListgroup from "./components/FDSListgroup";
import FDSHeaderfooter from "./components/FDSHeaderfooter";
import FDSModal from "./components/FDSModal";
import FDSPopover from "./components/FDSPopover";
import FDSTooltip from "./components/FDSTooltip";
import FDSProgressbar from "./components/FDSProgressbar";
import FDSSpinners from "./components/FDSSpinners";
import FDSInputgroup from "./components/FDSInputgroup";

import Sandbox from "./components/Sandbox";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      routePath: "/"
    };

    this.getRoute = this.getRoute.bind(this);
  }

  getRoute(rPath) {
    this.setState({ routePath: rPath });
  }

  render() {
    return (
      <div>
        <Router>
          <Switch>
              <Route
                path="/"
                exact
                render={props => (
                  <Fdsbootstrap {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/bootstrap/"
                exact
                render={props => (
                  <Fdsbootstrap {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/headerfooter/"
                exact
                render={props => (
                  <Routetest {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/getting-started/headerfooter/"
                exact
                render={props => (
                  <FDSHeaderfooter {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/layout/"
                exact
                render={props => (
                  <Layout {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/getstarted/"
                exact
                render={props => (
                  <Setup {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/accordion"
                exact
                render={props => (
                  <FDSAccordion {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/breadcrumb"
                exact
                render={props => (
                  <FDSBreadcrumb {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/badge"
                exact
                render={props => (
                  <FDSBadge {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/button"
                exact
                render={props => (
                  <FDSButton {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/card"
                exact
                render={props => (
                  <FDSCard {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/form"
                exact
                render={props => (
                  <FDSForms {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/link"
                exact
                render={props => (
                  <FDSLink {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/dropdown"
                exact
                render={props => (
                  <FDSDropdown {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/pagination"
                exact
                render={props => (
                  <FDSPagination {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/table"
                exact
                render={props => (
                  <FDSTable {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/tabs"
                exact
                render={props => (
                  <FDSTabs {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/getting-started/typography"
                exact
                render={props => (
                  <FDSTypography {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/getting-started/tokens"
                exact
                render={props => (
                  <FDSTokens {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/getting-started/grid"
                exact
                render={props => (
                  <FDSGrid {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/overflow"
                exact
                render={props => (
                  <FDSOverflow {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/datepicker"
                exact
                render={props => (
                  <FDSDatepicker {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/tag"
                exact
                render={props => (
                  <FDSTag {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/toast"
                exact
                render={props => (
                  <FDSToast {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/listgroup"
                exact
                render={props => (
                  <FDSListgroup {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/modal"
                exact
                render={props => (
                  <FDSModal {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/popovers"
                exact
                render={props => (
                  <FDSPopover {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/tooltips"
                exact
                render={props => (
                  <FDSTooltip {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/progressbar"
                exact
                render={props => (
                  <FDSProgressbar {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/spinners"
                exact
                render={props => (
                  <FDSSpinners {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/inputgroup"
                exact
                render={props => (
                  <FDSInputgroup {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/components/sandbox"
                exact
                render={props => (
                  <Sandbox {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                path="/lunchandlearn/"
                exact
                render={props => (
                  <Lunchandlearn {...props} routeUpdate={this.getRoute} />
                )}
              />
              <Route
                exact
                render={props => (
                  <Fdsbootstrap {...props} routeUpdate={this.getRoute} />
                )}
              />
              
          </Switch>
        </Router>
        <div className="footer">
          <div className="container">
            <div className="row">
              <div className="col-lg-7">
                <p>&copy; 2019 Airlines Reporting Corporation</p>
                <p>
                  SENSITIVE AND CONFIDENTIAL. Data presented or downloaded here
                  may contain ARC-sensitive information. Disclosure to 3rd
                  parties is prohibited without prior written consent of ARC.
                </p>
              </div>
              <div className="col-lg-5">
                <div className="footer-links">
                  <a className="fds-link" href="#">
                    Terms Of Use
                  </a>
                  <a className="fds-link" href="#">
                    Privacy Policy
                  </a>
                  <a className="fds-link" href="#">
                    Contact Us
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default hot(module)(App);

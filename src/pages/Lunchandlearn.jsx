import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";

class Lunchandlearn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Navbar expand="sm">
          <Container>
            <Navbar.Brand href="#home">
              <img
                src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
                height="32"
                className="d-inline-block align-baseline"
                alt="Airlines Reporting Corporation Logo"
              />
              <div className="navbar-brand-text">Online Training</div>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <NavDropdown title="Dropdown Link" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">
                    Something
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
              <div className="navbar-utility">
                <a href="" className="navbar-utility-link navbar-utility-icon">
                  <i className="fds-button__icon fds-glyphs-bell2"></i>
                </a>
                <a
                  href="#"
                  className="navbar-utility-link navbar-utility-username"
                >
                  User Name
                </a>
                <a href="#" className="navbar-utility-link">
                  Help
                </a>
                <a href="#" className="navbar-utility-link">
                  Close
                </a>
              </div>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <div style={{ minHeight: "80vh", background: "#fcfcfc" }}>
          <div className="form-jumbo">
            <div className="form-header">
              <h1 className="type-heading-h1-xl-on-dark">ARC Training</h1>
              <h3 className="type-heading-h3-primary-on-dark">
                Thank you for your interest in custom training from ARC. Please
                fill out the form and an ARC representative will contact you
                within two business days.
              </h3>
            </div>
          </div>
          <div className="pageform">
            <div className="container ">
              <div className="inner-form">
                <div className="row pt-3">
                  <div className="col-md-12">
                    <div className="online-training-levels mt-5">
                      <h3 className="type-heading-h3-secondary">
                        Select your tier:
                      </h3>
                      <div className="row mt-3">
                        <div className="col-md-4">
                          <Card className="text-center ">
                            <Card.Header className="type-heading-h2">
                              Tier 1
                            </Card.Header>
                            <Card.Body>
                              <Card.Title className="type-heading-h3-primary">
                                On-Demand Video Webinars
                              </Card.Title>
                              <Card.Text>
                                ARC’s on-demand webinars cover a wide range of
                                travel industry initiatives, including airline
                                distribution, fraud prevention, security,
                                payments and tips to grow your business.
                              </Card.Text>
                              <Button variant="secondary">Select Tier 1</Button>
                            </Card.Body>
                            <Card.Footer className="text-muted">
                              <div className="row">
                                <div className="col-md-6">$5000/yr</div>
                                <div className="col-md-6">$450/mo</div>
                              </div>
                            </Card.Footer>
                          </Card>
                        </div>
                        <div className="col-md-4">
                          <Card className="text-center">
                            <Card.Header className="type-heading-h2">
                              Tier 2
                            </Card.Header>
                            <Card.Body>
                              <Card.Title className="type-heading-h3-primary">
                                Live Webinars
                              </Card.Title>
                              <Card.Text>
                                Lorem ipsum dolor sit, amet consectetur
                                adipisicing elit. Ea exercitationem assumenda
                                vero et similique nostrum, voluptas aut incidunt
                                repudiandae molestias, nam in?
                              </Card.Text>
                              <Button variant="primary">Select Tier 2</Button>
                            </Card.Body>
                            <Card.Footer className="text-muted">
                              <div className="row">
                                <div className="col-md-6">$10,000/yr</div>
                                <div className="col-md-6">$900/mo</div>
                              </div>
                            </Card.Footer>
                          </Card>
                        </div>
                        <div className="col-md-4">
                          <Card className="text-center">
                            <Card.Header className="type-heading-h2">
                              Tier 3
                            </Card.Header>
                            <Card.Body>
                              <Card.Title className="type-heading-h3-primary">
                                Custom 1-on-1 Training
                              </Card.Title>
                              <Card.Text>
                                Lorem ipsum dolor sit, amet consectetur
                                adipisicing elit. Ea exercitationem assumenda
                                vero et similique nostrum, voluptas aut incidunt
                                repudiandae molestias, nam in?
                              </Card.Text>
                              <Button variant="success">Select Tier 3</Button>
                            </Card.Body>
                            <Card.Footer className="text-muted">
                              <div className="row">
                                <div className="col-md-6">$15,000/yr</div>
                                <div className="col-md-6">$1,350/mo</div>
                              </div>
                            </Card.Footer>
                          </Card>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-md-12">
                        <h3 className="type-heading-h3-secondary mb-5">
                          Contact Information:
                        </h3>
                        <Form>
                          <div className="row">
                            <div className="col-md-6">
                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="email" placeholder="" />
                              </Form.Group>

                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control type="email" placeholder="" />
                              </Form.Group>

                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="" />
                                <Form.Text className="text-muted">
                                  We'll never share your email with anyone else.
                                </Form.Text>
                              </Form.Group>

                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="email" placeholder="" />
                              </Form.Group>

                              <Form.Group controlId="formBasicCheckbox">
                                <Form.Label>Training Needs</Form.Label>
                                <Form.Check
                                  type="checkbox"
                                  label="Accreditation Tool"
                                />
                                <Form.Check
                                  type="checkbox"
                                  label="Credit Card Processing"
                                />
                                <Form.Check
                                  type="checkbox"
                                  label="Interactive Agent Reporting (IAR)"
                                />
                                <Form.Check
                                  type="checkbox"
                                  label="ARC Memo Manager (AMM)"
                                />
                                <Form.Check type="checkbox" label="ARC Pay" />
                              </Form.Group>
                            </div>
                            <div className="col-md-6">
                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>Company</Form.Label>
                                <Form.Control type="email" placeholder="" />
                              </Form.Group>

                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>
                                  ARC Number (if applicable)
                                </Form.Label>
                                <Form.Control type="email" placeholder="" />
                              </Form.Group>

                              <Form.Group controlId="formBasicEmail">
                                <Form.Label>Phone Number</Form.Label>
                                <Form.Control type="email" placeholder="" />
                              </Form.Group>

                              <Form.Group controlId="exampleForm.ControlTextarea1">
                                <Form.Label>Additional comments</Form.Label>
                                <Form.Control as="textarea" rows="3" />
                              </Form.Group>

                              <Form.Check
                                type="switch"
                                id="custom-switch-this"
                                label="Sign me up for our monthly newsletter"
                              />
                            </div>
                          </div>
                          <Button variant="primary" type="submit">
                            Submit
                          </Button>
                        </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Lunchandlearn;

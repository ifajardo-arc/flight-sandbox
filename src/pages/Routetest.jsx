import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";

class Routetest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Navbar expand="sm">
          <Container>
            <Navbar.Brand href="#home">
              <img
                src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
                width="65"
                height="24"
                className="d-inline-block align-baseline"
                alt="Airlines Reporting Corporation Logo"
              />
              React-Bootstrap
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <Nav.Link href="#link">Link</Nav.Link>
                <NavDropdown title="Dropdown Link" id="basic-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">
                    Something
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
              <div className="navbar-utility">
                <a href="" className="navbar-utility-link navbar-utility-icon">
                  <i className="fds-button__icon fds-glyphs-bell2"></i>
                </a>
                <a
                  href="#"
                  className="navbar-utility-link navbar-utility-username"
                >
                  User Name
                </a>
                <a href="#" className="navbar-utility-link">
                  Help
                </a>
                <a href="#" className="navbar-utility-link">
                  Close
                </a>
              </div>
            </Navbar.Collapse>
          </Container>
        </Navbar>

        <div style={{ height: "80vh", background: "#f8f8f8" }}>
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div class="input-group mt-5 mb-3">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Recipient's username"
                    aria-label="Recipient's username"
                    aria-describedby="button-addon2"
                  />
                  <div class="input-group-append">
                    <button
                      class="btn btn-outline-secondary"
                      type="button"
                      id="button-addon2"
                    >
                      <i class="fds-button__icon fds-glyphs-search"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Routetest;

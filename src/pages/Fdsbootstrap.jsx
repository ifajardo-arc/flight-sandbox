import React, { Component } from "react";
import { HashRouter as Router, Route, Link } from "react-router-dom";
import Prism from "prismjs";

//Component import
import Bootstrapnav from "../components/Bootstrapnav";
import Sidebar from "../components/Sidebar";

//Bootstrap React Imports
import Button from "react-bootstrap/Button";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import Card from "react-bootstrap/Card";

import Spinner from "react-bootstrap/Spinner";

class Fdsbootstrap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    Prism.highlightAll();
    this.props.routeUpdate(this.props.location.pathname);
  }

  render() {
    return (
      <div className="">
        <div className="row no-gutters">
          <div className="col-lg-2 col-md-3">
            <Bootstrapnav />
          </div>
          <div className="col-lg-8 col-md-6">
            <div className="px-5 py-3 mb-5">
              <div className="row">
                <div className="col-md-12">
                  <h1 className="type-heading-h1-xl pt-3">Introduction</h1>
                  <h2 className="type-heading-h2">
                    FDS Bootstrap is a toolkit for developing with HTML, CSS,
                    and JS using the Flight Design System by ARC. Use the Flight
                    Design System Sass variables and mixins, responsive grid
                    system, extensive prebuilt components, and powerful
                    components using React Bootstrap.
                  </h2>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <h3 className="type-heading-h3-primary pt-5">Quick Start</h3>

                  <p className="pt-3">
                    This is the{" "}
                    <a href="https://getbootstrap.com" target="_blank">
                      Bootstrap
                    </a>{" "}
                    implementation of ARC's Flight Design System created to be
                    compatible with{" "}
                    <a
                      target="_blank"
                      href="https://react-bootstrap.github.io/"
                    >
                      React Bootstrap
                    </a>
                    . This uses the flight-design-kit along with the
                    flight-design-system. The documetation site can be found at{" "}
                    <a
                      href="https://flightds.netlify.com/"
                      className="fds-link"
                      target="_blank"
                    >
                      https://flight-ds.netlify.com/
                    </a>
                    .
                  </p>
                  <p></p>
                  <h3 className="type-heading-h3-primary pt-3">CSS</h3>
                  <p>
                    To load FDS Bootstrap into your project, you must include
                    the project within your app. There are several ways to do
                    this:
                  </p>
                  <Tabs
                    defaultActiveKey="npm"
                    id="introduction-example"
                    transition={false}
                  >
                    <Tab eventKey="npm" title="NPM">
                      <p>
                        You may include this into your <code>package.json</code>{" "}
                        as a dependency to import select sass partials.
                      </p>
                      <pre>
                        <code className="language-bash">
                          {`$ npm install https://ifajardo-arc@bitbucket.org/ifajardo-arc/fds-bootstrap.git`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="css" title="HTML">
                      <p>
                        Copy-paste the stylesheet{" "}
                        <code className="language-html">{`<link>`}</code> into
                        your <code className="language-html">{`<head>`}</code>{" "}
                        before all other stylesheets to load our CSS.
                      </p>
                      <p>
                        This is recommended if you want to protoype quickly or
                        want to keep the css file unbundled.
                      </p>
                      <pre>
                        <code className="language-html">
                          {`<link rel="stylesheet" href="https://fds-bootstrap.netlify.com/dist/fds-bootstrap.min.css">
`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="git" title="Git">
                      <p>
                        You may fork this project to build upon it and import
                        into your project.{" "}
                      </p>
                      <pre>
                        <code className="language-bash">
                          {`$ git clone https://ifajardo-arc@bitbucket.org/ifajardo-arc/fds-bootstrap.git`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <h3 className="type-heading-h3-primary pt-3">JS</h3>
                  <p>
                    To utilize React Bootstrap, you must include it into your
                    project. If you already have React setup in your
                    environment, then you should import React-Bootstrap as a
                    dependency into your package.json.
                  </p>
                  <Tabs
                    defaultActiveKey="npm"
                    id="js-example"
                    transition={false}
                  >
                    <Tab eventKey="npm" title="NPM">
                      <p>
                        You may include this into your <code>package.json</code>{" "}
                        as a dependency to import select sass partials.
                      </p>
                      <pre>
                        <code className="language-bash">
                          {`$ npm install react-bootstrap bootstrap`}
                        </code>
                      </pre>
                    </Tab>
                    <Tab eventKey="js" title="JS">
                      <p>
                        Copy-paste the script{" "}
                        <code className="language-html">{`<script>`}</code> into
                        your project to utilize bootstrap react.
                      </p>
                      <p>
                        This is recommended if you want to protoype quickly or
                        want to keep the css file unbundled.
                      </p>
                      <pre>
                        <code className="language-html">
                          {`<script src="https://unpkg.com/react/umd/react.production.min.js" crossorigin />

<script
  src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"
  crossorigin
/>

<script
  src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js"
  crossorigin
/>
`}
                        </code>
                      </pre>
                    </Tab>
                  </Tabs>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <h3 className="type-heading-h3-primary pt-5">
                    Bootstrap FDS Components
                  </h3>
                  <p>
                    These components are fully compoatible with Bootstrap and
                    natively work with React Bootstrap:
                  </p>
                  <Link to="/components/accordion">Accordion</Link>,{" "}
                  <Link to="/components/badge">Badge</Link>,{" "}
                  <Link to="/components/breadcrumb">Breadcrumb</Link>,{" "}
                  <Link to="/components/button">Button</Link>,{" "}
                  <Link to="/components/card">Card</Link>,{" "}
                  <Link to="/components/dropdown">Dropdown</Link>,{" "}
                  <Link to="/components/link">Link</Link>,{" "}
                  <Link to="/components/pagination">Pagination</Link>,{" "}
                  <Link to="/components/tabs">Tabs</Link>,{" "}
                  <Link to="/components/form">Text Area</Link>,{" "}
                  <Link to="/components/form">Text Input</Link>,{" "}
                  <Link to="/components/form">Select</Link>
                  <p className="mt-3">
                    Text Input, Text Area, Select, Radio and Checkboxes have
                    been placed into the{" "}
                    <Link to="/components/form">Forms</Link> section. Floating
                    labels aren't supported in Bootsrap and React bootstrap so
                    the design has been modified for label tags in mind.
                  </p>
                </div>
                <div className="col-md-4">
                  <h3 className="type-heading-h3-primary pt-5">
                    FDS Components
                  </h3>
                  <p>
                    Some components from FDS don't have an implementation in
                    Bootstrap and therefore should be implemented in React and
                    refer to the respective documentation.
                  </p>
                  <ul className="fds-list fds-list--bulleted">
                    <li className="fds-list__item">
                      <Link to="/components/datepicker">Date Picker</Link>
                    </li>
                    <li className="fds-list__item">
                      <Link to="/components/overflow">Overflow Menu</Link>
                    </li>
                  </ul>
                </div>
                <div className="col-md-4">
                  <h3 className="type-heading-h3-primary pt-5">Fonts</h3>
                  <p>
                    Fonts must be included in your project for icons and fonts
                    to work correctly. Place the assets folder from the repo in
                    the same directory the fds-bootstrap css/scss will be
                    placed.
                  </p>
                  <a
                    target="_blank"
                    href=" https://bitbucket.org/ifajardo-arc/fds-bootstrap/src/master/assets/"
                    className="btn btn-primary"
                  >
                    Download Fonts
                  </a>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <h3 className="type-heading-h3-primary pt-5">
                    Design Tokens
                  </h3>
                  <p>
                    Sass variables within our design tokens should be used to
                    style components outside of the Flight Design System. These
                    should be used to influence colors, spacing, elevation and
                    more.
                  </p>
                  <Link
                    className="btn btn-primary"
                    to="/getting-started/tokens"
                  >
                    Learn More
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-md-6">
            <Sidebar />
          </div>
        </div>
      </div>
    );
  }
}

export default Fdsbootstrap;

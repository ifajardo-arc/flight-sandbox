import React, { Component } from "react";
import Prism from "prismjs";

import FDSButton from "../components/FDSButton";

class Setup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.buttonUpdate = this.buttonUpdate.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    Prism.highlightAll();
  }

  buttonUpdate(mytype, disabled) {
    console.log(disabled);
    if (mytype === "" || mytype == undefined) {
      mytype = this.state.buttonType;
    }

    if (disabled === "" || disabled == undefined) {
      disabled = this.state.buttonDisabled;
    }

    this.setState({ buttonType: mytype, buttonDisabled: disabled });
  }

  handleClick() {
    console.log("clicked");
    alert("Button Clicked!");
  }

  render() {
    return (
      <div className="paddingTopBottom">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="type-heading-h1-xl">Get Started</h1>
              <br />
              <p>
                This environment is for experimenting and testing with the
                flight-dev-kit. This is set up using webpack, reactjs, babel for
                ES6 support and the flight-dev-kit.
              </p>
              <p>
                Using the sandbox requires a developer to access to the ARC
                bitbucket and the appropriate access in Okta.
              </p>
              <p>
                Since the dev kit is still in development there are a couple
                workarounds using{" "}
                <code className="language-bash">npm link</code> that need to be
                done to make sure the latest version of the development branch
                is being pulled.
              </p>
              <h2 className="type-heading-h2">
                1. Setup SSH keys for bitbucket
              </h2>
              <p>
                Follow the instructions on bitbucket site to allow ssh access to
                be able to clone the repo onto your machine:{" "}
                <a
                  href="https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html"
                  target="_blank"
                >
                  Set up an SSH key
                </a>
              </p>
              <h2 className="type-heading-h2">2. Clone flight-dev-kit</h2>
              <p>
                Using <code className="language-bash">git</code> clone the
                flight-dev-kit repo to your desired directory.
              </p>
              <pre>
                <code className="language-bash">
                  {`$ git clone https://github.com/AirlinesReportingCorporation/fds-sandbox.git`}
                </code>
              </pre>
              <h2 className="type-heading-h2">
                3. Link flight-dev-kit development branch
              </h2>
              <pre>
                <code className="language-bash">
                  {`$ cd flight-dev-kit
$ git checkout development
$ npm install
$ npm link`}
                </code>
              </pre>
              <h2 className="type-heading-h2">
                4. Clone fds-sandbox and link flight-dev-kit
              </h2>
              <pre>
                <code className="language-bash">
                  {`$ git clone https://github.com/AirlinesReportingCorporation/fds-sandbox.git
$ npm install
$ npm link flight-dev-kit`}
                </code>
              </pre>
              <h2 className="type-heading-h2">
                5. Develop with flight-dev-kit
              </h2>
              <p>
                Run <code className="language-bash">{`npm start`}</code> to
                start a live dev reload server when changes to the /src
                directory is made.
              </p>
              <pre>
                <code className="language-bash">{`$ npm start`}</code>
              </pre>
              <p>
                Reference the{" "}
                <a href="https://flightds.netlify.com" target="_blank">
                  flight design website
                </a>{" "}
                to read documentation on usage, design tokens, components and
                more.{" "}
              </p>
              <p></p>
              <h2 className="type-heading-h2">6. Package for deployment</h2>
              <pre>
                <code className="language-bash">{`$ npm run build`}</code>
              </pre>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Setup;

import React, { Component } from "react";
import Prism from "prismjs";

import FDSButton from "../components/FDSButton";
import Spinner from "react-bootstrap/Spinner";

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonType: "primary",
      buttonDisabled: false
    };

    this.buttonUpdate = this.buttonUpdate.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    Prism.highlightAll();
  }

  buttonUpdate(mytype, disabled) {
    console.log(disabled);
    if (mytype === "" || mytype == undefined) {
      mytype = this.state.buttonType;
    }

    if (disabled === "" || disabled == undefined) {
      disabled = this.state.buttonDisabled;
    }

    this.setState({ buttonType: mytype, buttonDisabled: disabled });
  }

  handleClick() {
    console.log("clicked");
    alert("Button Clicked!");
  }

  render() {
    return (
      <div style={{ zIndex: "2" }}>
        <div className="homepage">
          <div className="container">
            <div className="row align-items-center">
              <div className="offset-lg-2 col-lg-8">
                <div className="homepage-intro">
                  <h1 className="type-heading-h1-xl">Flight Design System</h1>

                  <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                  </Spinner>

                  <h3 className="type-heading-h3-primary">
                    The Flight Design System is ARC's living design system. It
                    is currently in its infancy. As it matures, it will become
                    the single source of truth empowering ARC product teams to
                    build consistent, human-centered products and experiences.
                  </h3>
                  <pre style={{ fontSize: ".8rem", marginTop: "2rem" }}>
                    <code className="language-bash">
                      {`$ git clone https://ifajardo-arc@bitbucket.org/ifajardo-arc/fds-bootstrap.git`}
                    </code>
                  </pre>
                  <a
                    style={{ marginTop: "2rem" }}
                    href="/#/getstarted"
                    className="btn btn-success"
                  >
                    Get Started
                  </a>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card fds-card" style={{ marginTop: "1rem" }}>
                  <div className="card-body">
                    <h1 className="type-heading-h1">Get Started</h1>
                    <h6 className="card-subtitle mb-2 text-muted">
                      Installation
                    </h6>
                    <p className="card-text">
                      Everything you need to get up and running with the
                      sandbox.
                    </p>
                    <a href="/#/getstarted" className="btn btn-primary">
                      Learn More
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card fds-card" style={{ marginTop: "1rem" }}>
                  <div className="card-body">
                    <h1 className="type-heading-h1">Bootstrap</h1>
                    <h6 className="card-subtitle mb-2 text-muted">Framework</h6>
                    <p className="card-text">
                      View some example pages on how FDS works with bootstrap.
                    </p>
                    <a href="/#/bootstrap" className="btn btn-primary">
                      Learn More
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div className="card fds-card" style={{ marginTop: "1rem" }}>
                  <div className="card-body">
                    <h1 className="type-heading-h1">Layout</h1>
                    <h6 className="card-subtitle mb-2 text-muted">Examples</h6>
                    <p className="card-text">
                      View an example layout using bootstrap and FDS.
                    </p>
                    <a href="/#/layout/" className="btn btn-primary">
                      Learn More
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homepage;
